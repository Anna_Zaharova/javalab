package com.epam.newsmanagement.controller;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.newsmanagement.service.exception.ServiceException;

@ControllerAdvice
public class ExceptionController {

	private static final Logger logger = Logger
			.getLogger(ExceptionController.class);

	@ExceptionHandler(ServiceException.class)
	public String handleServiceException(ServiceException ex) {
		logger.error(ex);
		return "service-error-client";
	}

	@ExceptionHandler(Throwable.class)
	public String handleGeneralException(Throwable ex, Model model) {
		logger.error(ex);
		model.addAttribute("reason", ex.getMessage());
		return "general-error-client";
	}

	@ExceptionHandler(HttpSessionRequiredException.class)
	public String handleSessionException(Exception ex) {
		logger.error(ex);
		return "session-error";
	}
}