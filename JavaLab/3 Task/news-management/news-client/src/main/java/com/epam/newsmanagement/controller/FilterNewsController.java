package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.util.ConfigManager;
import com.epam.newsmanagement.vo.Filter;

@Controller
@SessionAttributes({ "filter" })
@RequestMapping("/")
public class FilterNewsController {
	@Autowired
	NewsManagementService newsManagementService;
	private final static int newsPerPage = Integer.valueOf(ConfigManager
			.getValue("newsPerPage"));

	@RequestMapping(value = "/newsList")
	public String findNews(@ModelAttribute("filter") Filter filter) {
		return "redirect:/newsList/1";
	}

	@RequestMapping(value = "/newsList/{pageNumber}", method = RequestMethod.GET)
	public String getNews(@PathVariable int pageNumber,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException {
		long count = newsManagementService.getNewsListByFilterCount(filter);
		int start = (pageNumber - 1) * newsPerPage + 1;
		model.addAttribute("newsListFilter", newsManagementService
				.getNewsListByFilter(filter, start, newsPerPage));
		model.addAttribute("newsCount", count);
		model.addAttribute("pageNumber", pageNumber);
		model.addAttribute("newsPerPage", newsPerPage);
		return "client-list";
	}

	@RequestMapping("/reset")
	public String reset(SessionStatus status) {
		status.setComplete();
		return "redirect:/newsList";
	}

	@ModelAttribute("tagList")
	public List<Tag> getAllTags() throws ServiceException {
		return newsManagementService.getAllTags();
	}

	@ModelAttribute("authorList")
	public List<Author> getAllAuthors() throws ServiceException {
		return newsManagementService.getAllAuthors();
	}

	@ModelAttribute("filter")
	public Filter getFilter() {
		return new Filter();
	}
}
