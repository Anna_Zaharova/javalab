<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/clear.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"
	type="text/javascript"></script>
<script src="<c:url value="/resources/js/custom.js" />"></script>
<title><tiles:getAsString name="title" /></title>
</head>
<body>
	<div class="wrapper">
		<tiles:insertAttribute name="header" />
		<div class="content-client">
			<tiles:insertAttribute name="body" />
		</div>
		<div class="appendix"></div>
	</div>
	<tiles:insertAttribute name="footer" />
</body>
</html>