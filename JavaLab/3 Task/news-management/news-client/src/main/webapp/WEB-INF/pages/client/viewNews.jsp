<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:url value="/newsList" var="newsList" />
<fmt:formatNumber
	value="${ ((position - (position-1)%newsPerPage+1)/newsPerPage)+0.5}"
	type="number" pattern="#" var="page" />
<a href="${newsList}/${page}" class="back"><spring:message
		code="page.back" /></a>
<div class="view-news">
	<div class="news-head">
		<div class="news-title">${news.title}</div>
	</div>
	<c:forEach items="${news.author }" var="author">
		<div class="news-author">(${author.name})</div>
	</c:forEach>
	<div class="news-create">
		<fmt:formatDate type="date" dateStyle="short"
			value="${news.modificationDate}" />
	</div>
	<div class="clear news-content">${news.fullText}</div>
	<div class="news-comment">
		<c:forEach items="${news.comments}" var="comment">
			<div class="comment-create">
				<fmt:formatDate type="date" dateStyle="short"
					value="${comment.creationDate}" />
			</div>
			<div class="comment-content">${comment.commentText}</div>
		</c:forEach>
		<c:url value="/viewNews/addComment/${news.id}" var="addComment" />
		<form:form action="${addComment}" modelAttribute="postComment"
			method="POST">
			<form:textarea cssClass="comment-area" path="commentText" />
			<input type="hidden" name="position" value="${position}">
			<input type="submit" class="post-comment-btn"
				value="<spring:message code="button.post.comment" />" />
		</form:form>
	</div>
</div>
<div class="clear"></div>
<c:if test="${position > 1}">
	<a href="<c:url value="/viewNews/position/${position - 1}"/>"
		class="prev"><spring:message code="page.previous" /></a>
</c:if>
<c:if test="${newsCount > position}">
	<a href="<c:url value="/viewNews/position/${position+1}"/>"
		class="next"><spring:message code="page.next" /></a>
</c:if>

