<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="header">
	<div class="clear"></div>
	<div class="language">
		<ul>
			<li><a href="?lang=en">EN</a></li>
			<li><a href="?lang=ru">RU</a></li>
		</ul>
	</div>
	<h1>
		<spring:message code="header.user" />
	</h1>
</div>