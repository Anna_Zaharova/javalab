<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<p>
	Page not found. Click <a href="<c:url value="/newsList"/>"> here</a> to
	return to main page.
</p>