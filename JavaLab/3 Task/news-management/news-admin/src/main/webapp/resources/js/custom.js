var currValue;
$(function() {
	$('#checkboxes span').click(function() {
		if (!$(event.target).is('#checkboxes [type=checkbox]')) {
			var checkbox = $(this).children();
			checkbox.prop("checked", !checkbox.prop("checked"));
		}
	});
	$('#checkboxes label').click(function() {
		$(this).prev().prop("checked", !$(this).prev().prop("checked"));
	});
	$('html').click(
			function() {
				if ((!$(event.target).is('#checkboxes input')
						&& !$(event.target).is('#checkboxes label') && !$(
						event.target).is('#checkboxes span'))
						&& $("#checkboxes").hasClass('open')) {
					$("#checkboxes").removeClass('open');
				} else if ($(event.target).is('#dropdown-tags select')
						&& !$("#checkboxes").hasClass('open')) {
					$("#checkboxes").addClass('open');
				}
			});
});

function editTag(el) {
	currValue = $(el).prev().prev().val();
	$(el).hide();
	$(el).nextUntil("div").show();
	$(el).prev().prev().prop('disabled', false);
	return false;
}

function updateTag(el) {
	updateURL = "/news-admin/admin/updateTag";
	$("#id").val($(el).prev().prev().val());
	$("#name").val($(el).prev().prev().prev().val());
	$("#tag-form").attr({
		action : updateURL
	});
	$("#tag-form").submit();
	return false;
}

function cancel(el) {
	$(el).hide();
	$(el).prevUntil("input").hide(); // hide delete
	$(el).prev().prev().prev().show();
	$(el).prev().prev().prev().prev().prev().val(currValue) // set previous
	$(el).prev().prev().prev().prev().prev().prop('disabled', true);
	return false;
}

function addTag(el) {
	addURL = "/news-admin/admin/addTag";
	$("#name").val($(el).prev().val());
	$("#tag-form").attr({
		action : addURL
	});
	$("#tag-form").submit();
	return false;
}

function editAuthor(el) {
	currValue = $(el).prev().prev().val();
	$(el).hide();
	$(el).nextUntil("div").show();
	$(el).prev().prev().prop('disabled', false);
	return false;
}

function addAuthor(el) {
	addURL = "/news-admin/admin/addAuthor";
	$("#id").val(0);
	$("#name").val($(el).prev().val());
	$("#author-form").attr({
		action : addURL
	});
	$("#author-form").submit();
	return false;
}

function updateAuthor(el) {
	updateURL = "/news-admin/admin/updateAuthor";
	$("#id").val($(el).prev().prev().val());
	$("#name").val($(el).prev().prev().prev().val());
	$("#author-form").attr({
		action : updateURL
	});
	$("#author-form").submit();
	return false;
}

function deleteNews(text) {
	if (confirm(text) == true) {
		return true;
	} else {
		return false;
	}
}
