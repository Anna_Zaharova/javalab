<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:url value="/admin/newsList" var="newsList" />
<c:url value="/admin/viewNews" var="viewNews" />
<fmt:formatNumber
	value="${ ((position - (position-1)%newsPerPage+1)/newsPerPage)+0.5}"
	type="number" pattern="#" var="page" />
<a href="${newsList}/${page}" class="back"><spring:message
		code="page.back" /></a>
<div class="view-news">
	<div class="news-head">
		<div class="news-title">
			<c:out value="${currentNews.title}" />
		</div>
	</div>
	<c:forEach items="${currentNews.author}" var="author">
		<div class="news-author">
			(
			<c:out value="${author.name}" />
			)
		</div>
	</c:forEach>
	<div class="news-create">
		<fmt:formatDate type="date" dateStyle="short"
			value="${currentNews.modificationDate}" />
	</div>
	<div class="clear news-content">
		<c:out value="${currentNews.fullText}" />
	</div>

	<div class="news-comment">
		<c:forEach items="${currentNews.comments}" var="comment"
			varStatus="status">
			<div class="comment-create">
				<fmt:formatDate type="date" dateStyle="short"
					value="${comment.creationDate}" />
			</div>
			<div class="comment-content">
				<a
					href="<c:url value="/admin/viewNews/deleteComment/${comment.id}?newsId=${currentNews.id}"/>"
					class="delete-comment-btn">&#9747;</a>
				<c:out value="${comment.commentText}" />
			</div>
		</c:forEach>
		<form:form action="${viewNews}/addComment/${currentNews.id}"
			modelAttribute="postComment" method="POST">
			<form:textarea cssClass="comment-area" path="commentText" />
			<form:errors path="commentText" cssClass="error" />
			<input type="submit" class="post-comment-btn"
				value="<spring:message code="button.post.comment" />" />
			<input type="hidden" name="position" value="${position}">
		</form:form>
	</div>
</div>
<div class="clear"></div>
<c:if test="${position > 1}">
	<a href="<c:url value="/admin/viewNews/position/${position-1}"/>"
		class="prev"><spring:message code="page.previous" /></a>
</c:if>
<c:if test="${newsCount > position}">
	<a href="<c:url value="/admin/viewNews/position/${position+1}"/>"
		class="next"><spring:message code="page.next" /></a>
</c:if>