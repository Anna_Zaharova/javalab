<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="edit-author-container">
	<form:form action="" modelAttribute="newAuthor" id="author-form"
		cssStyle='display:none' method="POST">
		<form:input path="name" />
		<form:input path="id" />
	</form:form>
	<div class="scroll">
		<c:forEach items="${authorList}" var="author">
			<div class="edit-author">
				<span class="author-title"><spring:message code="news.author" />:</span>
				<input type="text" class="author-name"
					value='<c:out value="${author.name}"/>' disabled /> <input
					type="hidden" value="${author.id}"> <a href=""
					onclick="return editAuthor(this)"><spring:message
						code="author.edit" /></a> <a href="" style='display: none;'
					onclick="return updateAuthor(this)"><spring:message
						code="author.update" /></a> <a
					href="<c:url value="/admin/expireAuthor/${author.id}"/>"
					style='display: none;'><spring:message code="author.expire" /></a>
				<a href="" style='display: none;' onclick="return cancel(this)"><spring:message
						code="author.cancel" /></a>
			</div>
		</c:forEach>
	</div>
	<div class="add-author">
		<span class="author-title"><spring:message
				code="news.add.author" />:</span> <input type="text" class="author-name"
			value="" /> <a href="" onclick="return addAuthor(this)"><spring:message
				code="button.save" /></a>
	</div>


</div>