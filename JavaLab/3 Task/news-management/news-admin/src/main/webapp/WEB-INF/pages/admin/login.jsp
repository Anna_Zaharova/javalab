<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="login-container">
	<c:if test='${not empty param.error}'>
		<div class="login-fields">
			<label style="color: red"><spring:message code="login.error" /></label>
			${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}
		</div>
	</c:if>
	<form method='POST' action="<c:url value="/j_spring_security_check" />">
		<div class="login-fields">
			<label for="login"><spring:message code="label.login" /></label> <input
				type="text" id="login" name="j_username" size="20" />
		</div>
		<div class="login-fields">
			<label for="password"><spring:message code="label.password" /></label>
			<input type="password" class="form-control" size="20" id="password"
				name="j_password" />
		</div>
		<div class="login-fields">
			<input type="submit" class="login-btn"
				value="<spring:message code="button.login" />" />
		</div>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
</div>