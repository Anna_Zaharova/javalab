<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:url value="/admin/addNews" var="addNews" />
<c:set var="now" value="<%=new java.util.Date()%>" />
<fmt:formatDate dateStyle="short" value="${now}" var="nowDate" />
<div class="add-news-container">
	<form:form action="${addNews }" modelAttribute="complexNews"
		method="POST">
		<div class="add-news-item">
			<span class=""><spring:message code="news.title" />:</span>
			<form:input path="title" />
			<form:errors path="title" cssClass="error" />
		</div>
		<div class="add-news-item">
			<span class=""><spring:message code="news.date" />:</span>
			<form:input path="creationDate" value="${nowDate}" />
			<form:errors path="creationDate" cssClass="error" />
		</div>
		<div class="add-news-item">
			<span class=""><spring:message code="news.brief" />:</span>
			<form:textarea path="shortText" cssClass="add-news-brief" />
			<form:errors path="shortText" cssClass="error" />
		</div>
		<div class="add-news-item">
			<span class=""><spring:message code="news.content" />:</span>
			<form:textarea path="fullText" cssClass="content-text" />
			<form:errors path="fullText" cssClass="error" />
		</div>
		<div class="author-and-tags">
			<div id="dropdown" class="news-list">
				<div id="dropdown-tags">
					<select><option><spring:message
								code="label.selectTag" /></option></select>
				</div>
				<div id="checkboxes">
					<div class="tag-list scroll-checkbox">
						<span><spring:message code="label.selectTag" /></span>
						<form:checkboxes items="${tagList}" path="tags" itemLabel="name"
							itemValue="id" />
					</div>
				</div>
			</div>
			<spring:message code="label.selectAuthor" var="selectAuthor" />
			<div class="news-list">
				<form:errors path="authorId" cssClass="error" />
				<form:select path="authorId">
					<form:option value="" label="${selectAuthor }" />
					<c:forEach items="${authorList}" var="author">
						<form:option value="${author.id}">${author.name}</form:option>
					</c:forEach>
				</form:select>
			</div>
		</div>
		<div class="clear"></div>
		<input type="submit" class="news-save-btn"
			value="<spring:message code="button.save" />" />
	</form:form>
</div>