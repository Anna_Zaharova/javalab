package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Controller
@RequestMapping("/admin")
public class EditTagsController {

	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping("/editTags")
	public String getEditTagsPage(ModelMap model) {
		model.addAttribute("newTag", new Tag());
		return "edit-tags-admin";
	}

	@RequestMapping(value = "/updateTag", method = RequestMethod.POST)
	public String updateTag(@ModelAttribute("newTag") Tag tag)
			throws ServiceException {
		newsManagementService.updateTag(tag);
		return "redirect:/admin/editTags";
	}

	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public String addTag(@Valid @ModelAttribute("newTag") Tag tag,
			BindingResult result) throws ServiceException {
		if (result.hasErrors()) {
			return "edit-tags-admin";
		}
		tag.setId(null);
		newsManagementService.addTag(tag);
		return "redirect:/admin/editTags";
	}

	@RequestMapping(value = "/deleteTag/{tagId}", method = RequestMethod.GET)
	public String deleteTag(@PathVariable Long tagId) throws ServiceException {
		newsManagementService.deleteTag(tagId);
		return "redirect:/admin/editTags";
	}

	@ModelAttribute("tagList")
	public List<Tag> getAllTags() throws ServiceException {
		return newsManagementService.getAllTags();
	}

}
