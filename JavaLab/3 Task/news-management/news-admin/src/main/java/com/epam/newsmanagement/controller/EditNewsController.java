package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utility.DateConversionUtility;
import com.epam.newsmanagement.vo.NewsVO;

@Controller
@RequestMapping("/admin/editNews")
public class EditNewsController {
	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping("/{newsId}")
	public String viewNews(@PathVariable("newsId") Long newsId, Model model)
			throws ServiceException {
		model.addAttribute("news", convertNewsToNewsVO(newsManagementService
				.getNewsWithoutComments(newsId)));
		return "edit-news-admin";
	}

	@RequestMapping("/update")
	public String updateNews(@ModelAttribute("news") NewsVO newsVO, Model model)
			throws ServiceException {
		newsVO.setLocale(LocaleContextHolder.getLocale().toString());
		newsVO.setModificationDate(new Date());
		try {
			newsManagementService.updateNews(newsVO);
		} catch (ServiceException ex) {
			model.addAttribute("news", newsVO);
			return "edit-news-admin";
		}
		return String.format("redirect:/admin/viewNews/position/%d",
				newsManagementService.getPosition(newsVO.getNewsId()));
	}

	@ModelAttribute("tagList")
	public List<Tag> getAllTags() throws ServiceException {
		return newsManagementService.getAllTags();
	}

	@ModelAttribute("authorList")
	public List<Author> getAllAuthors() throws ServiceException {
		return newsManagementService.getAllAuthors();
	}

	private NewsVO convertNewsToNewsVO(News news) {
		NewsVO newsVO = new NewsVO();
		newsVO.setAuthorId(news.getAuthor().iterator().next().getId());
		newsVO.setCreationDate(DateConversionUtility.convertFromDateToString(
				LocaleContextHolder.getLocale().toString(),
				news.getCreationDate()));
		newsVO.setFullText(news.getFullText());
		newsVO.setShortText(news.getShortText());
		newsVO.setTitle(news.getTitle());
		newsVO.setNewsId(news.getId());
		newsVO.setVersion(news.getVersion());
		List<Long> tags = new ArrayList<>();
		for (Tag tag : news.getTags()) {
			tags.add(tag.getId());
		}
		newsVO.setTags(tags);
		return newsVO;
	}
}
