package com.epam.newsmanagement.util;

import java.util.ResourceBundle;

public class ConfigManager {
	private static final ResourceBundle rb = ResourceBundle
			.getBundle("configuration");

	public static String getValue(String key) {
		return rb.getString(key);
	}
}
