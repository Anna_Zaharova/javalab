package com.epam.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@Transactional
@ActiveProfiles("jpa")
public class CommentDAOImplTest {

	@Autowired
	private CommentDAO commentDAO;

	@Test
	public void add() throws DAOException {
		Comment comment = new Comment();
		comment.setCommentText("text");
		comment.setCreationDate(new Date());

		Long id = commentDAO.add(comment);
		comment = commentDAO.fetch(id);
		assertTrue(comment.getId() == id);
		assertEquals(comment.getCommentText(), "text");
	}

	@Test
	public void fetch() throws DAOException {
		Comment comment = commentDAO.fetch(1l);
		assertTrue(comment.getId() == 1l);
		assertEquals(comment.getCommentText(), "Cool!");
	}

	@Test(expected = DAOException.class)
	public void delete() throws DAOException {
		try {
			commentDAO.delete(2l);
		} catch (DAOException ex) {
			fail();
		}
		commentDAO.fetch(2l);
	}

	@Test
	public void update() throws DAOException {
		Comment comment = commentDAO.fetch(2l);
		comment.setCommentText("qwerty");
		commentDAO.update(comment);
		comment = commentDAO.fetch(2l);
		assertTrue(comment.getId() == 2l);
		assertEquals(comment.getCommentText(), "qwerty");
	}

	@Test
	public void fetchAll() throws DAOException {
		List<Comment> comments = commentDAO.fetchAll();
		assertTrue(comments.size() == 4);
	}

	@Test
	public void fetchByNews() throws DAOException {
		List<Comment> comments = commentDAO.fetchByNews(1l);
		assertTrue(comments.size() == 2);
	}

}
