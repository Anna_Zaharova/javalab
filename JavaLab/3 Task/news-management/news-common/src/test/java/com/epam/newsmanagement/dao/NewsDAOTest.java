package com.epam.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.vo.Filter;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@SuppressWarnings("unchecked")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@Transactional
@ActiveProfiles("jpa")
public class NewsDAOImplTest {
	@Autowired
	private NewsDAO newsDAO;

	@Test
	public void add() throws DAOException {
		News news = initNews();
		Long id = newsDAO.add(news);
		news = newsDAO.fetch(id);
		assertEquals(news.getFullText(), "full");
		assertEquals(news.getTitle(), "title");
		assertEquals(news.getShortText(), "short");
		assertTrue(news.getVersion() == 0);
	}

	@Test
	public void update() throws DAOException {
		News news = newsDAO.fetch(1l);
		news.setTitle("qwerty");
		newsDAO.update(news);
		assertEquals(news.getFullText(), "full_text1");
		assertEquals(news.getTitle(), "qwerty");
		assertEquals(news.getShortText(), "short_text1");
		assertTrue(news.getVersion() == 1);
		assertTrue(news.getId() == 1l);
	}

	@Test(expected = DAOException.class)
	@Ignore
	public void delete() throws DAOException {
		try {
			newsDAO.delete(1l);
		} catch (DAOException ex) {
			fail();
		}
		newsDAO.delete(1l);
	}

	@Test
	public void fetchAll() throws DAOException {
		List<News> news = newsDAO.fetchAll();
		assertTrue(news.size() == 4);
	}

	@Test
	public void searchNotFound() throws DAOException {
		Filter filter = new Filter();
		filter.setAuthor(1l);
		filter.setTags(new ArrayList<Long>() {
			{
				add(1l);
				add(2l);
			}
		});
		List<News> news = newsDAO.search(filter, 1, 10);
		assertTrue(news.size() == 0);
	}

	@Test
	public void searchByAuthor() throws DAOException {
		Filter filter = new Filter();
		filter.setAuthor(2l);
		List<News> news = newsDAO.search(filter, 1, 10);
		assertTrue(news.size() == 1);
	}

	@Test
	public void searchByTag() throws DAOException {
		Filter filter = new Filter();
		filter.setTags(new ArrayList<Long>() {
			{
				add(1l);
				add(2l);
			}
		});
		List<News> news = newsDAO.search(filter, 1, 10);
		assertTrue(news.size() == 2);
	}

	@Test
	public void searchByAuthorAndTags() throws DAOException {
		Filter filter = new Filter();
		filter.setAuthor(2l);
		filter.setTags(new ArrayList<Long>() {
			{
				add(1l);
				add(2l);
			}
		});
		List<News> news = newsDAO.search(filter, 1, 10);
		assertTrue(news.size() == 1);
	}

	@Test
	public void checkOrder() throws DAOException {
		Filter filter = new Filter();
		List<News> news = newsDAO.search(filter, 1, 10);
		// System.out.println(news);
		// assertTrue(news.get(0).getCount() == 2);
		assertEquals(news.get(0).getTitle(), "title2");
	}

	@Test
	public void get() throws DAOException {
		News news = newsDAO.fetch(2l);
		assertEquals(news.getFullText(), "full_text2");
		assertEquals(news.getTitle(), "title2");
		assertEquals(news.getShortText(), "short_text2");
		assertTrue(news.getVersion() == 0);
		assertTrue(news.getId() == 2l);
	}

	private News initNews() {
		News news = new News();
		news.setFullText("full");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		news.setShortText("short");
		news.setTitle("title");
		news.setVersion(0);
		return news;
	}

}
