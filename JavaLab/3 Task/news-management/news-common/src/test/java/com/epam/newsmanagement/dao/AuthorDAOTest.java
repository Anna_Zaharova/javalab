package com.epam.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@Transactional
@ActiveProfiles("jpa")
public class AuthorDAOImplTest {

	@Autowired
	private AuthorDAO authorDAO;

	@Test
	public void add() throws DAOException {
		Author author = new Author();
		author.setName("Olaf");
		Long id = authorDAO.add(author);
		assertTrue(id > 0);
		author = authorDAO.fetch(id);
		assertEquals(author.getName(), "Olaf");
	}

	@Test(expected = DAOException.class)
	public void delete() throws DAOException {
		try {
			authorDAO.delete(3l);
		} catch (DAOException ex) {
			fail();
		}
		authorDAO.fetch(3l);
	}

	@Test
	public void update() throws DAOException {
		Author author = authorDAO.fetch(2l);
		assertEquals(author.getName(), "Helen");
		author.setName("Author");
		authorDAO.update(author);
		author = authorDAO.fetch(2l);
		assertEquals(author.getName(), "Author");
	}

	@Test
	public void fetch() throws DAOException {
		Author author = authorDAO.fetch(1l);
		assertTrue(author.getId() == 1l);
		assertEquals(author.getName(), "Petr");
		assertTrue(author.getExpired() == null);
	}

	@Test
	public void fetchAll() throws DAOException {
		List<Author> authors = authorDAO.fetchAll();
		assertTrue(authors.size() == 4);
		assertEquals(authors.get(0).getName(), "Artem");
		assertTrue(authors.get(0).getId() == 4l);
	}

	@Test
	@Ignore
	public void fetchNotExpired() throws DAOException {
		List<Author> authors = authorDAO.fetchNotExpired();
		System.out.println(authors);
		assertTrue(authors.size() == 3);
		assertEquals(authors.get(0).getName(), "Petr");
		assertTrue(authors.get(0).getId() == 1l);
	}

	@Test
	public void fetchByNews() throws DAOException {
		Author author = authorDAO.fetchByNews(1l);
		assertEquals(author.getName(), "Helen");
		assertTrue(author.getId() == 2l);
	}

}
