package com.epam.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@Transactional
@ActiveProfiles("jpa")
public class TagDAOImplTest {
	@Autowired
	private TagDAO tagDAO;

	@Test
	public void add() throws DAOException {
		Tag tag = new Tag();
		tag.setName("Hope");
		Long id = tagDAO.add(tag);
		assertTrue(id > 0);
		tag = tagDAO.fetch(id);
		assertEquals(tag.getName(), "Hope");
	}

	@Test
	public void fetch() throws DAOException {
		Tag tag = tagDAO.fetch(1l);
		assertTrue(tag.getId() == 1l);
		assertEquals(tag.getName(), "Java");
	}

	@Test
	public void update() throws DAOException {
		Tag tag = tagDAO.fetch(2l);
		assertEquals(tag.getName(), ".NET");
		tag.setName("TAG");
		tagDAO.update(tag);
		tag = tagDAO.fetch(2l);
		assertEquals(tag.getName(), "TAG");
	}

	@Test(expected = DAOException.class)
	public void delete() throws DAOException {
		try {
			tagDAO.delete(3l);
		} catch (DAOException ex) {
			fail();
		}
		tagDAO.fetch(3l);
	}

	@Test
	public void fetchAll() throws DAOException {
		List<Tag> newsList = tagDAO.fetchAll();
		assertTrue(newsList.size() == 4);
	}

	@Test
	public void fetchList() throws DAOException {
		List<Tag> newsList = tagDAO.fetch(Arrays.asList(1l, 2l));
		assertTrue(newsList.size() == 2);
	}

	@Test
	public void fetchByNews() throws DAOException {
		List<Tag> newsList = tagDAO.fetchByNews(1l);
		assertTrue(newsList.size() == 2);
	}
}