package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.vo.Filter;

public interface NewsService {
	/**
	 * Edits information about the news
	 * 
	 * @param news
	 *            news to edit
	 * @throws ServiceException
	 */
	void update(News news) throws ServiceException;

	/**
	 * Returns the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the news
	 * @throws ServiceException
	 */
	News get(Long newsId) throws ServiceException;

	/**
	 * Deletes the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @throws ServiceException
	 */
	void delete(Long newsId) throws ServiceException;

	/**
	 * Deletes news by identifiers
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws ServiceException
	 */
	void delete(List<Long> newsId) throws ServiceException;

	/**
	 * Adds the news to the data source
	 * 
	 * @param news
	 *            news to add
	 * @return the news identifier
	 * @throws ServiceException
	 */
	Long add(News news) throws ServiceException;

	/**
	 * Returns all news from data source
	 * 
	 * @return the list of news
	 * @throws ServiceException
	 */
	List<News> getAll() throws ServiceException;

	/**
	 * Returns news list satisfied the requirements of the filter
	 * 
	 * @param filter
	 *            requirements that must be met by each news
	 * @param firstNews
	 *            number of first news
	 * @param lastNews
	 *            number of the last news
	 * @return news list
	 * @throws ServiceException
	 */
	List<News> search(Filter filter, int firstNews, int lastNews)
			throws ServiceException;

	/**
	 * Returns total news number
	 * 
	 * @param filter
	 *            requirements that must be met by each news on the page
	 * @return total news number
	 * @throws ServiceException
	 */
	long getNewsCount(Filter filter) throws ServiceException;

	/**
	 * Returns news position
	 * 
	 * @param newsId
	 *            the news identifiers
	 * @return position (min position = 1)
	 * @throws ServiceException
	 */
	int getPosition(Long newsId) throws ServiceException;
}
