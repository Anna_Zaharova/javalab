package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Service
public class TagServiceImp implements TagService {
	@Autowired
	private TagDAO tagDAO;

	public TagServiceImp() {

	}

	public TagServiceImp(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		try {
			tagDAO.update(tag);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void delete(Long tagId) throws ServiceException {
		try {
			tagDAO.delete(tagId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void add(Tag tag) throws ServiceException {
		try {
			tagDAO.add(tag);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Tag> getByNews(Long newsId) throws ServiceException {
		try {
			return tagDAO.fetchByNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Tag> getAll() throws ServiceException {
		try {
			return tagDAO.fetchAll();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Tag> get(List<Long> ids) throws ServiceException {
		try {
			return tagDAO.fetch(ids);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}
}
