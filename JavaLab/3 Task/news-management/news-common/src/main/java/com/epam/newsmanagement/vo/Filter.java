package com.epam.newsmanagement.vo;

import java.util.List;

public class Filter {
	private List<Long> tags;
	private Long author;

	public Long getAuthor() {
		return author;
	}

	public void setAuthor(Long author) {
		this.author = author;
	}

	public List<Long> getTags() {
		return tags;
	}

	public void setTags(List<Long> tags) {
		this.tags = tags;
	}

}
