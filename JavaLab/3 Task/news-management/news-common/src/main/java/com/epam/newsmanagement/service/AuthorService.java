package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface AuthorService {
	/**
	 * Adds the author to the data source
	 * 
	 * @param author
	 *            author to add
	 * @throws ServiceException
	 */
	void add(Author author) throws ServiceException;

	/**
	 * Deletes the author by identifier
	 * 
	 * @param authorId
	 *            the author identifier
	 * @throws ServiceException
	 */
	void delete(Long authorId) throws ServiceException;

	/**
	 * Edits the information about the author
	 * 
	 * @param author
	 *            author to edit
	 * @throws ServiceException
	 */
	void update(Author author) throws ServiceException;

	/**
	 * Returns the author by identifier
	 * 
	 * @param authorId
	 *            the author identifier
	 * @return the author
	 * @throws ServiceException
	 */
	Author get(Long authorId) throws ServiceException;

	/**
	 * Returns the author by news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the author
	 * @throws ServiceException
	 */
	Author getByNews(Long newsId) throws ServiceException;

	/**
	 * Returns all existing authors
	 * 
	 * @return authors list
	 * @throws ServiceException
	 */
	List<Author> getAll() throws ServiceException;

	/**
	 * Returns authors not expired
	 * 
	 * @return authors list
	 * @throws ServiceException
	 */
	List<Author> getNotExpired() throws ServiceException;
}
