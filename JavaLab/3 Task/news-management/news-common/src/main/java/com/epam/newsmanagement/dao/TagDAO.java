package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Tag;

/**
 * Provides extra operations for manipulate tag data from the data source
 * 
 *
 */
public interface TagDAO extends AbstractDAO<Tag, Long> {
	/**
	 * Receives the list of tags by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @return the list of tags
	 * @throws DAOException
	 */
	List<Tag> fetchByNews(Long newsId) throws DAOException;

	List<Tag> fetch(List<Long> ids) throws DAOException;
}
