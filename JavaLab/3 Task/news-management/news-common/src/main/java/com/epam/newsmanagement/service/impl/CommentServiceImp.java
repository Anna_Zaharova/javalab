package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Service
public class CommentServiceImp implements CommentService {
	@Autowired
	private CommentDAO commentDAO;

	public CommentServiceImp() {
	}

	public CommentServiceImp(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	@Override
	public void delete(Long commentId) throws ServiceException {
		try {
			commentDAO.delete(commentId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void add(Comment comment) throws ServiceException {
		try {
			commentDAO.add(comment);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Comment> getByNews(Long newsId) throws ServiceException {
		try {
			return commentDAO.fetchByNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

}
