package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Service
public class AuthorServiceImp implements AuthorService {
	@Autowired
	private AuthorDAO authorDAO;

	public AuthorServiceImp() {

	}

	public AuthorServiceImp(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	@Override
	public void add(Author author) throws ServiceException {
		try {
			authorDAO.add(author);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void delete(Long authorId) throws ServiceException {
		try {
			authorDAO.delete(authorId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void update(Author author) throws ServiceException {
		try {
			authorDAO.update(author);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public Author get(Long authorId) throws ServiceException {
		try {
			return authorDAO.fetch(authorId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public Author getByNews(Long newsId) throws ServiceException {
		try {
			return authorDAO.fetchByNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Author> getAll() throws ServiceException {
		try {
			return authorDAO.fetchAll();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Author> getNotExpired() throws ServiceException {
		try {
			return authorDAO.fetchNotExpired();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}
}
