package com.epam.newsmanagement.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentVO {
	@Size(min = 1, max = 100)
	@NotNull
	private String commentText;
	private Long newsId;

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

}
