package com.epam.newsmanagement.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Tag;

//@Repository
public class TagDAOImp implements TagDAO {
	private final static String JPQL_GET_ALL = "select tag from Tag tag order by tag.name asc";
	private final static String JPQL_GET_BY_NEWS = "select tag from Tag tag join tag.news news where news.id = :id order by tag.name";
	private final static String JPQL_GET_LIST = "select tag from Tag tag where tag.id in :ids";
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long add(Tag tag) throws DAOException {
		try {

			entityManager.persist(tag);
			return tag.getId();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public Tag fetch(Long id) throws DAOException {
		try {
			Tag tag = entityManager.find(Tag.class, id);
			if (tag == null) {
				throw new DAOException(String.format(
						"the tag  with id = %d not found", id));
			}
			return tag;
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void update(Tag tag) throws DAOException {
		try {
			entityManager.merge(tag);
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		try {
			Tag tag = entityManager.find(Tag.class, id);
			entityManager.remove(tag);
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<Tag> fetchAll() throws DAOException {
		try {
			TypedQuery<Tag> query = entityManager.createQuery(JPQL_GET_ALL,
					Tag.class);
			return query.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<Tag> fetchByNews(Long newsId) throws DAOException {
		try {
			TypedQuery<Tag> query = entityManager.createQuery(JPQL_GET_BY_NEWS,
					Tag.class);
			query.setParameter("id", newsId);
			return query.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<Tag> fetch(List<Long> ids) throws DAOException {
		try {
			TypedQuery<Tag> query = entityManager.createQuery(JPQL_GET_LIST,
					Tag.class);
			query.setParameter("ids", ids);
			return query.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

}
