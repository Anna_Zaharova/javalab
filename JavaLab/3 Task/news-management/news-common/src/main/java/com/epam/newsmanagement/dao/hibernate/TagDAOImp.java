package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Tag;

@Repository
public class TagDAOImp implements TagDAO {
	private static final String HQL_DELETE = "delete from Tag where id = :id";
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long add(Tag tag) throws DAOException {
		try {
			return (Long) sessionFactory.getCurrentSession().save(tag);
		} catch (HibernateException he) {
			throw new DAOException("the tag not created", he);
		}
	}

	@Override
	public Tag fetch(Long id) throws DAOException {
		Tag tag = null;
		try {
			if ((tag = (Tag) sessionFactory.getCurrentSession().get(Tag.class,
					id)) == null) {
				throw new DAOException(String.format(
						"the tag  with id = %d not found", id));
			}
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
		return tag;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> fetch(List<Long> ids) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Tag.class);
		criteria.add(Restrictions.in("id", ids));
		return criteria.list();
	}

	@Override
	public void update(Tag tag) throws DAOException {
		try {
			Tag tag1 = (Tag) sessionFactory.getCurrentSession().load(Tag.class,
					tag.getId());
			tag1.setName(tag.getName());
			sessionFactory.getCurrentSession().flush();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Query query = sessionFactory.getCurrentSession()
				.createQuery(HQL_DELETE);
		query.setParameter("id", id);
		if (query.executeUpdate() != 1) {
			throw new DAOException(String.format(
					"the tag with id = %d not deleted", id));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> fetchAll() throws DAOException {
		try {
			return sessionFactory.getCurrentSession().createCriteria(Tag.class)
					.addOrder(Order.asc("name")).list();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> fetchByNews(Long newsId) throws DAOException {
		try {
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(Tag.class, "tag");
			criteria.createAlias("tag.news", "news");
			criteria.add(Restrictions.eq("news.id", newsId));
			return criteria.list();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}
}
