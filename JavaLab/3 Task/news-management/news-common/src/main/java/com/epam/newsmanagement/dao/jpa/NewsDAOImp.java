package com.epam.newsmanagement.dao.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.vo.Filter;

//@Repository
public class NewsDAOImp implements NewsDAO {
	private static final String JPQL_DELETE_LIST = "delete from News news where news.id IN :ids";
	private static final String JPQL_GET_ALL = "select news from News news";
	private static final String SQL_GET_POSITION = "SELECT RN as position FROM (SELECT N.news_id,"
			+ " row_number() OVER ( ORDER BY C.comments_amount DESC NULLS LAST, N.modification_date DESC)"
			+ " RN FROM News N LEFT OUTER JOIN (SELECT news_id , COUNT(*) as comments_amount FROM Comments"
			+ " GROUP BY news_id) C ON N.news_id = C.news_id) WHERE news_id = ?";
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long add(News news) throws DAOException {
		try {
			entityManager.persist(news);
			return news.getId();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public News fetch(Long id) throws DAOException {
		try {
			News news = entityManager.find(News.class, id);
			if (news == null) {
				throw new DAOException(String.format(
						"the news with id = %d not found", id));
			}
			return news;
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void update(News news) throws DAOException {
		try {
			entityManager.merge(news);
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		try {
			News news = entityManager.find(News.class, id);
			if (news == null) {
				throw new DAOException(String.format(
						"the news with id = %d not found", id));
			}
			entityManager.remove(news);
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<News> fetchAll() throws DAOException {
		try {
			TypedQuery<News> query = entityManager.createQuery(JPQL_GET_ALL,
					News.class);
			return query.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<News> search(Filter filter, int firstNews, int count)
			throws DAOException {
		try {
			List<Predicate> predicates = new ArrayList<Predicate>();

			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteria = builder.createQuery(News.class);
			Root<News> newsRoot = criteria.from(News.class);
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				Predicate predicate = newsRoot.join("tags").get("id")
						.in(filter.getTags());
				predicates.add(predicate);
			}
			if (filter.getAuthor() != null && filter.getAuthor() != 0) {
				Predicate predicate = builder.equal(newsRoot.join("author")
						.get("id"), filter.getAuthor());
				predicates.add(predicate);
			}

			criteria.where(predicates.toArray(new Predicate[predicates.size()]));
			criteria.orderBy(builder.desc(builder.size(newsRoot
					.<Collection<Comment>> get("comments"))), builder
					.desc(newsRoot.get("modificationDate")));
			CriteriaQuery<News> select = criteria.select(newsRoot).distinct(
					true);
			TypedQuery<News> typedQuery = entityManager.createQuery(select);

			typedQuery.setFirstResult(firstNews - 1);
			typedQuery.setMaxResults(count);

			return typedQuery.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public long fetchTotalNews(Filter filter) throws DAOException {
		try {
			List<Predicate> predicates = new ArrayList<Predicate>();

			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<News> newsRoot = criteria.from(News.class);
			criteria.select(builder.countDistinct(newsRoot));
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				Join<News, Tag> joinTag = newsRoot.join("tags");
				Expression<String> exp = joinTag.get("id");
				Predicate predicate = exp.in(filter.getTags());
				predicates.add(predicate);
			}
			if (filter.getAuthor() != null && filter.getAuthor() != 0) {
				Join<News, Author> joinAuthor = newsRoot.join("author");
				Expression<String> exp = joinAuthor.get("id");
				Predicate predicate = exp.in(filter.getAuthor());
				predicates.add(predicate);
			}

			criteria.where(predicates.toArray(new Predicate[predicates.size()]));
			return entityManager.createQuery(criteria).getSingleResult();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void delete(List<Long> newsId) throws DAOException {
		try {
			Query query = entityManager.createQuery(JPQL_DELETE_LIST);
			query.setParameter("ids", newsId);
			query.executeUpdate();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public int fetchPosition(Long newsId) throws DAOException {
		try {
			Query query = entityManager.createNativeQuery(SQL_GET_POSITION);
			query.setParameter(1, newsId);
			return ((BigDecimal) query.getSingleResult()).intValue();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}
}
