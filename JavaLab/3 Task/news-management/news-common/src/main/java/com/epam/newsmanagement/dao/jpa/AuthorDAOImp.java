package com.epam.newsmanagement.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;

//@Repository
public class AuthorDAOImp implements AuthorDAO {
	private static final String JPQL_GET_ALL = "select author from Author author order by author.name ASC";
	private static final String JQPL_GET_BY_NEWS = "select author from Author author join author.news news where news.id = :id";
	private static final String JQPL_GET_NOT_EXPIRED = "select author from Author author where author.expired is null order by author.name ASC";
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long add(Author author) throws DAOException {
		entityManager.persist(author);
		return author.getId();
	}

	@Override
	public Author fetch(Long id) throws DAOException {
		try {
			Author author = entityManager.find(Author.class, id);
			if (author == null) {
				throw new DAOException(String.format(
						"the author with id = %d not found", id));
			}
			return author;
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void update(Author author) throws DAOException {
		try {
			entityManager.merge(author);
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		try {
			Author author = entityManager.find(Author.class, id);
			if (author == null) {
				throw new DAOException(String.format(
						"the author with id = %d not found", id));
			}
			entityManager.remove(author);
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<Author> fetchAll() throws DAOException {
		try {
			TypedQuery<Author> query = entityManager.createQuery(JPQL_GET_ALL,
					Author.class);
			return query.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}

	}

	@Override
	public Author fetchByNews(Long newsId) throws DAOException {
		try {
			TypedQuery<Author> query = entityManager.createQuery(
					JQPL_GET_BY_NEWS, Author.class);
			query.setParameter("id", newsId);
			return query.getSingleResult();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<Author> fetchNotExpired() throws DAOException {
		try {
			TypedQuery<Author> query = entityManager.createQuery(
					JQPL_GET_NOT_EXPIRED, Author.class);
			return query.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}
}
