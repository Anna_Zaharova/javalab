package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Comment;

@Repository
public class CommentDAOImp implements CommentDAO {
	private final static String HQL_DELETE = "delete from Comment where id = :id";
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long add(Comment comment) throws DAOException {
		try {
			return (Long) sessionFactory.getCurrentSession().save(comment);
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@Override
	public Comment fetch(Long id) throws DAOException {
		Comment comment = null;
		try {
			if ((comment = (Comment) sessionFactory.getCurrentSession().get(
					Comment.class, id)) == null) {
				throw new DAOException(String.format(
						"the comment with id = %d not found", id));
			}
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
		return comment;
	}

	@Override
	public void update(Comment comment) throws DAOException {
		try {
			sessionFactory.getCurrentSession().merge(comment);
		} catch (HibernateException he) {
			throw new DAOException(String.format(
					"the comment with id = %d not updated", comment.getId()));
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					HQL_DELETE);
			query.setParameter("id", id);
			if (query.executeUpdate() != 1) {
				throw new DAOException(String.format(
						"the comment with id = %d not deleted", id));
			}
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> fetchAll() throws DAOException {
		try {
			return sessionFactory.getCurrentSession()
					.createCriteria(Comment.class)
					.addOrder(Order.desc("creationDate")).list();
		} catch (HibernateException ex) {
			throw new DAOException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> fetchByNews(Long newsId) throws DAOException {
		try {
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(Comment.class, "comment");
			criteria.add(Restrictions.eq("comment.news.id", newsId));
			criteria.addOrder(Order.asc("comment.creationDate"));
			return criteria.list();
		} catch (HibernateException ex) {
			throw new DAOException(ex);
		}
	}
}
