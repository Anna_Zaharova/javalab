package com.epam.newsmanagement.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Comment;

//@Repository
public class CommentDAOImp implements CommentDAO {
	private static final String JPQL_GET_ALL = "select comment from Comment comment";
	private static final String JQPL_GET_BY_NEWS = "select comment from Comment comment join comment.news news where news.id = :id order by comment.creationDate asc";
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long add(Comment comment) throws DAOException {
		try {
			entityManager.persist(comment);
			return comment.getId();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public Comment fetch(Long id) throws DAOException {
		try {
			Comment comment = entityManager.find(Comment.class, id);
			if (comment == null) {
				throw new DAOException(String.format(
						"the comment with id = %d not found", id));
			}
			return comment;
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void update(Comment comment) throws DAOException {
		try {
			entityManager.merge(comment);
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}

	}

	@Override
	public void delete(Long id) throws DAOException {
		try {
			Comment comment = entityManager.find(Comment.class, id);
			if (comment == null) {
				throw new DAOException(String.format(
						"the comment with id = %d not found", id));
			}
			entityManager.remove(comment);
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<Comment> fetchAll() throws DAOException {
		try {
			TypedQuery<Comment> query = entityManager.createQuery(JPQL_GET_ALL,
					Comment.class);
			return query.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<Comment> fetchByNews(Long newsId) throws DAOException {
		try {
			TypedQuery<Comment> query = entityManager.createQuery(
					JQPL_GET_BY_NEWS, Comment.class);
			query.setParameter("id", newsId);
			return query.getResultList();
		} catch (PersistenceException ex) {
			throw new DAOException(ex);
		}
	}

}
