package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;

@Repository
public class AuthorDAOImp implements AuthorDAO {
	private static final String HQL_DELETE = "delete from Author where id = :id";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long add(Author author) throws DAOException {
		try {
			return (Long) sessionFactory.getCurrentSession().save(author);
		} catch (HibernateException he) {
			throw new DAOException("the author not created", he);
		}
	}

	@Override
	public Author fetch(Long id) throws DAOException {
		Author author = null;
		try {
			if ((author = (Author) sessionFactory.getCurrentSession().get(
					Author.class, id)) == null) {
				throw new DAOException(String.format(
						"the author with id = %d not found", id));
			}
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
		return author;
	}

	@Override
	public void update(Author author) throws DAOException {
		try {
			sessionFactory.getCurrentSession().merge(author);
		} catch (HibernateException he) {
			throw new DAOException(String.format(
					"the author with id = %d not updated", author.getId()), he);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					HQL_DELETE);
			query.setParameter("id", id);
			if (query.executeUpdate() != 1) {
				throw new DAOException(String.format(
						"the author with id = %d not deleted", id));
			}
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> fetchAll() throws DAOException {
		try {
			return sessionFactory.getCurrentSession()
					.createCriteria(Author.class).addOrder(Order.asc("name"))
					.list();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> fetchNotExpired() throws DAOException {
		try {
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(Author.class);
			criteria.add(Restrictions.isNull("expired"));
			criteria.addOrder(Order.asc("name"));
			return criteria.list();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@Override
	public Author fetchByNews(Long newsID) throws DAOException {
		try {
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(Author.class, "author");
			criteria.createAlias("author.news", "news");
			criteria.add(Restrictions.eq("news.id", newsID));
			return (Author) criteria.uniqueResult();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}
}
