package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;

/**
 * 
 * Represents common dao operations
 * 
 * @param <E>
 *            - entity class
 * @param <PK>
 *            - type of identifier
 */
public interface AbstractDAO<E, PK> {
	/**
	 * Inserts new entity into the data source. After inserting an entity
	 * receives the identifier
	 * 
	 * @param entity
	 *            entity which must be inserted
	 * @return the identifier of inserted entity
	 * @throws DAOException
	 */
	PK add(E entity) throws DAOException;

	/**
	 * Receives an entity from the data source by identifier
	 * 
	 * @param id
	 *            the identifier of entity
	 * @return an entity which was found
	 * @throws DAOException
	 */
	E fetch(PK id) throws DAOException;

	/**
	 * Updates the information about entity
	 * 
	 * @param entity
	 *            an entity for update
	 * @throws DAOException
	 */
	void update(E entity) throws DAOException;

	/**
	 * Deletes the information about entity
	 * 
	 * @param id
	 *            the identifier of entity
	 * @throws DAOException
	 */
	void delete(PK id) throws DAOException;

	/**
	 * Receives all data from the data source
	 * 
	 * @return entity list
	 * @throws DAOException
	 */
	List<E> fetchAll() throws DAOException;
}
