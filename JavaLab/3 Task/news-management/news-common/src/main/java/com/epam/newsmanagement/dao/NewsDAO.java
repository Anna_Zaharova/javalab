package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.vo.Filter;

/**
 * Provides extra operations for manipulate News data from the data source
 * 
 *
 */
public interface NewsDAO extends AbstractDAO<News, Long> {

	/**
	 * Returns list of news by filter
	 * 
	 * @param firstNews
	 * @param lastNews
	 * @return
	 * @throws DAOException
	 */
	List<News> search(Filter filter, int firstNews, int lastNews)
			throws DAOException;

	/**
	 * Returns total news number satisfied the requirements of filter
	 * 
	 * @param filter
	 *            requirements that must be met by the news
	 * @return total news number
	 * @throws DAOException
	 */
	long fetchTotalNews(Filter filter) throws DAOException;

	/**
	 * Deletes news by news identifiers
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws DAOException
	 */
	void delete(List<Long> newsId) throws DAOException;

	int fetchPosition(Long newsId) throws DAOException;
}
