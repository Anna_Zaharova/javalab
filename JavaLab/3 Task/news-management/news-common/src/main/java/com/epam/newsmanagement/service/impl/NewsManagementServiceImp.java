package com.epam.newsmanagement.service.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utility.DateConversionUtility;
import com.epam.newsmanagement.vo.CommentVO;
import com.epam.newsmanagement.vo.Filter;
import com.epam.newsmanagement.vo.NewsVO;

@Service
@Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRES_NEW)
public class NewsManagementServiceImp implements NewsManagementService {
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;

	public NewsManagementServiceImp() {

	}

	public NewsManagementServiceImp(NewsService newsService,
			CommentService commentService, TagService tagService,
			AuthorService authorService) {
		this.authorService = authorService;
		this.commentService = commentService;
		this.newsService = newsService;
		this.tagService = tagService;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	@Override
	public Long addNewsWithAuthorAndTags(NewsVO newsVO) throws ServiceException {
		News news = convertNewsVOtoNewsTO(newsVO);
		final Long authorId = newsVO.getAuthorId();
		Set<Author> authors = new HashSet<Author>() {
			{
				add(authorService.get(authorId));
			}
		};
		news.setAuthor(authors);
		if (newsVO.getTags() != null && !newsVO.getTags().isEmpty()) {
			news.setTags(new HashSet(tagService.get(newsVO.getTags())));
		}
		return newsService.add(news);
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public News getNewsWithoutComments(Long newsId) throws ServiceException {
		return newsService.get(newsId);
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<News> getNewsListByFilter(Filter filter, int start, int count)
			throws ServiceException {
		List<News> news = newsService.search(filter, start, count);
		for (News ns : news) {
			ns.setComments(commentService.getByNews(ns.getId()));
		}
		return news;
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public News getNews(Long newsId) throws ServiceException {
		News news = newsService.get(newsId);
		news.setComments(commentService.getByNews(newsId));
		return news;
	}

	@Override
	public void deleteCompleNewsListById(List<Long> newsId)
			throws ServiceException {
		newsService.delete(newsId);
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public long getNewsListByFilterCount(Filter filter) throws ServiceException {
		return newsService.getNewsCount(filter);
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public int getPosition(Long newsId) throws ServiceException {
		return newsService.getPosition(newsId);
	}

	@Override
	public void updateNews(NewsVO newsVO) throws ServiceException {
		News news = convertNewsVOtoNewsTO(newsVO);
		final Long authorId = newsVO.getAuthorId();
		news.setAuthor(new HashSet<Author>() {
			{
				add(authorService.get(authorId));
			}
		});
		if (newsVO.getTags() != null && !newsVO.getTags().isEmpty()) {
			news.setTags(new HashSet(tagService.get(newsVO.getTags())));
		}
		newsService.update(news);
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Author> getAllAuthors() throws ServiceException {
		return authorService.getAll();
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Tag> getAllTags() throws ServiceException {
		return tagService.getAll();
	}

	@Override
	public List<Author> getAuthorsNotExpired() throws ServiceException {
		return authorService.getNotExpired();
	}

	@Override
	public void addAuthor(Author author) throws ServiceException {
		authorService.add(author);
	}

	@Override
	public void updateAuthor(Author author) throws ServiceException {
		authorService.update(author);
	}

	@Override
	public void updateTag(Tag tag) throws ServiceException {
		tagService.update(tag);
	}

	@Override
	public void addTag(Tag tag) throws ServiceException {
		tagService.add(tag);
	}

	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		tagService.delete(tagId);
	}

	@Override
	public void addComment(CommentVO commentVO) throws ServiceException {
		commentService.add(convertCommentVOtoComment(commentVO));
	}

	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		commentService.delete(commentId);
	}

	@Override
	public void setAuthorExpired(Long id) throws ServiceException {
		Author author = authorService.get(id);
		author.setExpired(new Date());
		authorService.update(author);
	}

	private News convertNewsVOtoNewsTO(NewsVO newsVO) throws ServiceException {
		News news = new News();
		try {
			Date creation = DateConversionUtility.convertFromStringToDate(
					newsVO.getLocale(), newsVO.getCreationDate());
			news.setCreationDate(creation);
			news.setFullText(newsVO.getFullText());
			if (newsVO.getModificationDate() == null) {
				news.setModificationDate(news.getCreationDate());
			} else {
				news.setModificationDate(newsVO.getModificationDate());
			}
			if (newsVO.getNewsId() != null) {
				news.setId(newsVO.getNewsId());
			}
			news.setShortText(newsVO.getShortText());
			news.setTitle(newsVO.getTitle());
			news.setVersion(newsVO.getVersion());
		} catch (ParseException ex) {
			throw new ServiceException(ex);
		}
		return news;
	}

	private Comment convertCommentVOtoComment(CommentVO commentVO)
			throws ServiceException {
		Comment comment = new Comment();
		comment.setCommentText(commentVO.getCommentText());
		comment.setCreationDate(new Date());
		comment.setNews(newsService.get(commentVO.getNewsId()));
		return comment;
	}
}
