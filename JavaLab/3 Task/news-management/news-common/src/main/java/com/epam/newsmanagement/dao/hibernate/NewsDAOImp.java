package com.epam.newsmanagement.dao.hibernate;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.vo.Filter;

@Repository
public class NewsDAOImp implements NewsDAO {
	private static final String SQL_GET_POSITION = "SELECT RN as position FROM (SELECT N.news_id,"
			+ " row_number() OVER ( ORDER BY C.comments_amount DESC NULLS LAST, N.modification_date DESC)"
			+ " RN FROM News N LEFT OUTER JOIN (SELECT news_id , COUNT(*) as comments_amount FROM Comments"
			+ " GROUP BY news_id) C ON N.news_id = C.news_id) WHERE news_id = :id";
	private static final String HQL_DELETE_LIST = "delete from News where id in (:ids)";
	private static final String HQL_GET_ALL = "select distinct news from News news order by size(news.comments) desc, news.modificationDate desc";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long add(News news) throws DAOException {
		try {
			return (Long) sessionFactory.getCurrentSession().save(news);
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@Override
	public News fetch(Long id) throws DAOException {
		News news = null;
		try {
			if ((news = (News) sessionFactory.getCurrentSession().get(
					News.class, id)) == null) {
				throw new DAOException(String.format(
						"the news with id = %d not found", id));
			}
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
		return news;
	}

	@Override
	public void update(News news) throws DAOException {
		try {
			sessionFactory.getCurrentSession().merge(news);
		} catch (HibernateException he) {
			throw new DAOException(String.format(
					"the news with id = %d not updated", news.getId()));
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		try {
			News news = (News) sessionFactory.getCurrentSession().get(
					News.class, id);
			sessionFactory.getCurrentSession().delete(news);
			sessionFactory.getCurrentSession().flush();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@Override
	public void delete(List<Long> ids) throws DAOException {
		try {
			sessionFactory.getCurrentSession().createQuery(HQL_DELETE_LIST)
					.setParameterList("ids", ids).executeUpdate();

		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> fetchAll() throws DAOException {
		try {
			return sessionFactory.getCurrentSession().createQuery(HQL_GET_ALL)
					.list();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> search(Filter filter, int start, int count)
			throws DAOException {
		try {
			StringBuilder hqlSearch = new StringBuilder(
					"select distinct news from News news left join news.tags tag left join news.author author where");
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(News.class);
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				hqlSearch.append(" tag.id in (:tags)");
			} else {
				hqlSearch.append(" 1 = 1");
			}
			hqlSearch.append(" and ");
			if (filter.getAuthor() != null && filter.getAuthor() != 0) {
				hqlSearch.append(" author.id = :author");
			} else {
				hqlSearch.append(" 1 = 1");
			}
			hqlSearch
					.append(" order by size(news.comments) desc, news.modificationDate desc");
			Query query = sessionFactory.getCurrentSession().createQuery(
					hqlSearch.toString());
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				query.setParameterList("tags", filter.getTags());
			}
			if (filter.getAuthor() != null && filter.getAuthor() != 0) {
				query.setParameter("author", filter.getAuthor());
			}
			query.setFirstResult(start - 1);
			query.setMaxResults(count);
			criteria.setFirstResult(start - 1);
			criteria.setMaxResults(count);

			criteria.addOrder(Order.desc("count"));
			criteria.addOrder(Order.desc("modificationDate"));

			return query.list();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@Override
	public long fetchTotalNews(Filter filter) throws DAOException {
		try {
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(News.class, "news");
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				Criteria tags = criteria.createAlias("tags", "tag");
				tags.add(Restrictions.in("tag.id", filter.getTags()));
			}
			if (filter.getAuthor() != null && filter.getAuthor() != 0) {
				Criteria author = criteria.createAlias("author", "author");
				author.add(Restrictions.eq("author.id", filter.getAuthor()));
			}
			criteria.setProjection(Projections.rowCount()).uniqueResult();
			return (Long) criteria.uniqueResult();
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@Override
	public int fetchPosition(Long newsId) throws DAOException {
		return ((BigDecimal) sessionFactory.getCurrentSession()
				.createSQLQuery(SQL_GET_POSITION).setParameter("id", newsId)
				.uniqueResult()).intValue();
	}

}
