package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.vo.Filter;

@Service
public class NewsServiceImp implements NewsService {
	@Autowired
	private NewsDAO newsDAO;

	public NewsServiceImp() {
	}

	public NewsServiceImp(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {

			newsDAO.delete(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void delete(List<Long> newsId) throws ServiceException {
		try {
			newsDAO.delete(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void update(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public News get(Long newsId) throws ServiceException {
		try {
			return newsDAO.fetch(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public Long add(News news) throws ServiceException {
		try {
			return newsDAO.add(news);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<News> getAll() throws ServiceException {
		try {
			return newsDAO.fetchAll();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<News> search(Filter filter, int firstNews, int lastNews)
			throws ServiceException {
		try {
			return newsDAO.search(filter, firstNews, lastNews);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public long getNewsCount(Filter filter) throws ServiceException {
		try {
			return newsDAO.fetchTotalNews(filter);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public int getPosition(Long newsId) throws ServiceException {
		try {
			return newsDAO.fetchPosition(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}
}
