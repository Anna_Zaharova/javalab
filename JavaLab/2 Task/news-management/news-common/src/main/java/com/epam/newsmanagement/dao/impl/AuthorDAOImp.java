package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.utility.DAOUtility;

@Component
public class AuthorDAOImp implements AuthorDAO {

	private final static String SQL_READ_NEWS_AUTHOR = "SELECT  A.author_id, A.author_name, A.expired FROM Author A INNER JOIN News_Author NA ON"
			+ " A.author_id = NA.author_id WHERE NA.news_id = ?";
	private final static String SQL_INSERT_AUTHOR = "INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, ?)";
	private final static String SQL_READ_AUTHOR = "SELECT author_id, author_name, expired  FROM Author WHERE author_id = ?";
	private final static String SQL_UPDATE_AUTHOR = "UPDATE Author SET author_name = ? WHERE author_id = ?";
	private final static String SQL_DELETE_AUTHOR = "DELETE FROM Author WHERE author_id = ?";
	private final static String SQL_SET_EXPIRED = "UPDATE Author SET expired = ? WHERE author_id = ?";
	private final static String SQL_READ_ALL_AUTHORS = "SELECT author_id, author_name, expired FROM Author ORDER BY author_name";
	private final static String SQL_READ_AUTHORS_NOT_EXPIRED = "SELECT author_id, author_name, expired FROM Author WHERE expired IS NULL ORDER BY author_name";
	@Autowired
	private DataSource dataSource;

	@Override
	public Author fetchByNews(Long newsID) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_READ_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsID);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createAuthor(resultSet);
			}
			throw new DAOException(String.format(
					"the author with newsID = %d not found", newsID));
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public Author fetch(Long authorId) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_AUTHOR);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createAuthor(resultSet);
			} else {
				throw new DAOException(String.format(
						"the author with id = %d not found", authorId));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public void delete(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR);
			preparedStatement.setLong(1, authorId);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(String.format(
						"the author with id = %d not deleted", authorId));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public Long add(Author author) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		long id = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHOR,
					new String[] { "author_id" });
			preparedStatement.setString(1, author.getName());
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the author not created");
			}
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
				author.setId(id);
			} else {
				throw new DAOException("the key not generated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return id;
	}

	@Override
	public void update(Author author) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			preparedStatement.setString(1, author.getName());
			preparedStatement.setLong(2, author.getId());
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(String.format(
						"the author with id = %d not updated", author.getId()));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void setExpired(Author author) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SET_EXPIRED);
			preparedStatement.setTimestamp(1, new Timestamp(author.getExpired()
					.getTime()));
			preparedStatement.setLong(2, author.getId());
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(
						String.format(
								"the expired value of the author with id = %d not inserted",
								author.getId()));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public List<Author> fetchAll() throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_ALL_AUTHORS);
			List<Author> authors = new ArrayList<>();
			while (resultSet.next()) {
				authors.add(createAuthor(resultSet));
			}
			return authors;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection, statement,
					resultSet);
		}
	}

	@Override
	public List<Author> fetchNotExpired() throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_AUTHORS_NOT_EXPIRED);
			List<Author> authors = new ArrayList<>();
			while (resultSet.next()) {
				authors.add(createAuthor(resultSet));
			}
			return authors;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection, statement,
					resultSet);
		}
	}

	private Author createAuthor(ResultSet resultSet) throws SQLException {
		Author author = new Author();
		author.setId(resultSet.getLong("author_id"));
		author.setName(resultSet.getString("author_name"));
		author.setExpired(resultSet.getTimestamp("expired"));
		return author;
	}
}
