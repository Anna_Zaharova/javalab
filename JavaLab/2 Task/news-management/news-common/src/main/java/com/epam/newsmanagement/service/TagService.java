package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface TagService {

	/**
	 * Edits the information about the tag
	 * 
	 * @param tag
	 *            tag to edit
	 * @throws ServiceException
	 */
	void update(Tag tag) throws ServiceException;

	/**
	 * Deletes the tag by identifier
	 * 
	 * @param tagId
	 *            the tag identifier
	 * @throws ServiceException
	 */
	void delete(Long tagId) throws ServiceException;

	/**
	 * Adds the tag to the data source
	 * 
	 * @param tag
	 *            tag to add
	 * @throws ServiceException
	 */
	void add(Tag tag) throws ServiceException;

	/**
	 * Receives the list of tags by news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the list of tags
	 * @throws ServiceException
	 */
	List<Tag> getByNews(Long newsId) throws ServiceException;

	/**
	 * Returns all existing tags
	 * 
	 * @return tags list
	 * @throws ServiceException
	 */
	List<Tag> getAll() throws ServiceException;

	/**
	 * Checks whether the tag belongs to some news
	 * 
	 * @param tagId
	 *            tag identifier
	 * @return true if tag belong to some news, false - otherwise
	 * @throws ServiceException
	 */
	boolean isUsed(Long tagId) throws ServiceException;
}
