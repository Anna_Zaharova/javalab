package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.utility.DAOUtility;

@Component
public class TagDAOImp implements TagDAO {
	private final static String SQL_READ_NEWS_TAG = "SELECT T.tag_id, T.tag_name FROM Tag T INNER JOIN News_Tag NT"
			+ " ON T.tag_id = NT.tag_id WHERE NT.news_id = ? ORDER BY T.tag_name";
	private final static String SQL_INSERT_TAG = "INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, ?)";
	private final static String SQL_UPDATE_TAG = "UPDATE Tag SET tag_name = ? WHERE tag_id = ?";
	private final static String SQL_DELETE_TAG = "DELETE FROM Tag WHERE tag_id = ?";
	private final static String SQL_READ_TAG = "SELECT tag_id, tag_name FROM Tag WHERE tag_id = ?";
	private final static String SQL_READ_ALL_TAGS = "SELECT tag_id, tag_name  FROM Tag ORDER BY tag_name";
	private final static String SQL_IS_TAG_USED = "SELECT count(*) FROM News_Tag WHERE tag_id=?";
	@Autowired
	private DataSource dataSource;

	@Override
	public List<Tag> fetchByNews(Long newsId) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_NEWS_TAG);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			List<Tag> tags = new ArrayList<>();
			while (resultSet.next()) {
				tags.add(createTag(resultSet));
			}
			return tags;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public Long add(Tag tag) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		long id = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_TAG,
					new String[] { "tag_id" });
			preparedStatement.setString(1, tag.getName());
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the tag not created");
			}
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
				tag.setId(id);
			} else {
				throw new DAOException("the key not generated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return id;
	}

	@Override
	public Tag fetch(Long id) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_TAG);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createTag(resultSet);
			} else {
				throw new DAOException(String.format(
						"the tag  with id = %d not found", id));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public void update(Tag tag) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
			preparedStatement.setString(1, tag.getName());
			preparedStatement.setLong(2, tag.getId());
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(String.format(
						"the tag with id = %d not updated", tag.getId()));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(String.format(
						"the tag with id = %d not deleted", id));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<Tag> fetchAll() throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_ALL_TAGS);
			List<Tag> tags = new ArrayList<>();
			while (resultSet.next()) {
				tags.add(createTag(resultSet));
			}
			return tags;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection, statement,
					resultSet);
		}
	}

	@Override
	public boolean isUsed(Long tagId) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_IS_TAG_USED);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				if (resultSet.getInt(1) > 0)
					return true;
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return false;
	}

	private Tag createTag(ResultSet resultSet) throws SQLException {
		Tag tag = new Tag();
		tag.setId(resultSet.getLong("tag_id"));
		tag.setName(resultSet.getString("tag_name"));
		return tag;
	}

}
