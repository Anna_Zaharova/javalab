package com.epam.newsmanagement.entity;

import java.util.List;

public class User extends Entity {

	private static final long serialVersionUID = -7730133643461947266L;
	private String userName;
	private String login;
	private String password;
	private List<String> roles;

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public User() {

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return super.toString() + "\nuserName = " + userName + "\nlogin = "
				+ login + "\npassword = " + password + "\nroles = " + roles;
	}

	@Override
	public int hashCode() {
		int hash = 17, result = 0;
		int superHash = super.hashCode();
		result = hash * result + ((login == null) ? 0 : login.hashCode());
		result = hash * result + superHash
				+ +((password == null) ? 0 : password.hashCode());
		result = hash * result * ((roles == null) ? 0 : roles.hashCode());
		result = hash * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (roles == null) {
			if (other.roles != null)
				return false;
		} else if (!roles.equals(other.roles))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

}
