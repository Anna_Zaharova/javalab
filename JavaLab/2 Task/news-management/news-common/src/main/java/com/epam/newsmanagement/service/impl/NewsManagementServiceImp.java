package com.epam.newsmanagement.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.ComplexNews;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utility.DateConversionUtility;
import com.epam.newsmanagement.vo.Filter;
import com.epam.newsmanagement.vo.NewsVO;

@Service
@Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRES_NEW)
public class NewsManagementServiceImp implements NewsManagementService {
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;

	public NewsManagementServiceImp() {

	}

	public NewsManagementServiceImp(NewsService newsService,
			CommentService commentService, TagService tagService,
			AuthorService authorService) {
		this.authorService = authorService;
		this.commentService = commentService;
		this.newsService = newsService;
		this.tagService = tagService;
	}

	@Override
	public Long addNewsWithAuthorAndTags(NewsVO newsVO) throws ServiceException {
		News news = convertNewsVOtoNewsTO(newsVO);
		newsService.add(news);
		newsService.addAuthor(news.getId(), newsVO.getAuthorId());
		if (newsVO.getTags() != null && !newsVO.getTags().isEmpty()) {
			newsService.addTags(news.getId(), newsVO.getTags());
		}
		return news.getId();
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@Override
	public ComplexNews getComplexNews(Long newsId) throws ServiceException {
		ComplexNews complexNews = new ComplexNews();
		complexNews.setNews(newsService.get(newsId));
		addNewsInfo(complexNews);
		return complexNews;
	}

	@Override
	public void deleteCompleNewsListById(List<Long> newsId)
			throws ServiceException {
		commentService.deleteByNews(newsId);
		newsService.deleteAuthor(newsId);
		newsService.deleteTag(newsId);
		newsService.delete(newsId);
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@Override
	public int getNewsListByFilterCount(Filter filter) throws ServiceException {
		return newsService.getNewsCount(filter);
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@Override
	public List<ComplexNews> getNewsListByFilter(Filter filter, int start,
			int end) throws ServiceException {
		List<ComplexNews> news = new ArrayList<>(end - start + 1);
		List<News> newsList = newsService.search(filter, start, end);

		for (News ns : newsList) {
			ComplexNews complexNews = new ComplexNews();
			complexNews.setNews(ns);
			addNewsInfo(complexNews);
			news.add(complexNews);
		}
		return news;
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@Override
	public int getPosition(Long newsId) throws ServiceException {
		return newsService.getPosition(newsId);
	}

	@Override
	public void updateComplexNews(NewsVO newsVO) throws ServiceException {
		News news = convertNewsVOtoNewsTO(newsVO);
		Long newsId = news.getId();
		newsService.update(news);
		newsService.updateAuthorByNews(newsId, newsVO.getAuthorId());
		if (!tagService.getByNews(newsId).isEmpty()) {
			newsService.deleteTagByNews(newsId);
		}
		if (newsVO.getTags() != null && !newsVO.getTags().isEmpty()) {
			newsService.addTags(newsId, newsVO.getTags());
		}
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@Override
	public List<Author> getAllAuthors() throws ServiceException {
		return authorService.getAll();
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@Override
	public List<Tag> getAllTags() throws ServiceException {
		return tagService.getAll();
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@Override
	public List<Author> getAuthorsNotExpired() throws ServiceException {
		return authorService.getNotExpired();
	}

	@Override
	public void addAuthor(Author author) throws ServiceException {
		authorService.add(author);
	}

	@Override
	public void setExpired(Author author) throws ServiceException {
		authorService.setExpired(author);
	}

	@Override
	public void updateAuthor(Author author) throws ServiceException {
		authorService.update(author);
	}

	@Override
	public void updateTag(Tag tag) throws ServiceException {
		tagService.update(tag);
	}

	@Override
	public void addTag(Tag tag) throws ServiceException {
		tagService.add(tag);
	}

	@Override
	public void deleteTagById(Long tagId) throws ServiceException {
		if (tagService.isUsed(tagId)) {
			newsService.deleteTag(tagId);
		}
		tagService.delete(tagId);
	}

	@Override
	public void addComment(Comment comment) throws ServiceException {
		commentService.add(comment);
	}

	@Override
	public void deleteCommentById(Long commentId) throws ServiceException {
		commentService.delete(commentId);
	}

	private void addNewsInfo(ComplexNews complexNews) throws ServiceException {
		Long newsId = complexNews.getNews().getId();
		complexNews.setAuthor(authorService.getByNews(newsId));
		complexNews.setTags(tagService.getByNews(newsId));
		complexNews.setComments(commentService.getByNews(newsId));
	}

	private News convertNewsVOtoNewsTO(NewsVO newsVO) throws ServiceException {
		News news = new News();
		try {
			Date creation = DateConversionUtility.convertFromStringToDate(
					newsVO.getLocale(), newsVO.getCreationDate());
			news.setCreationDate(creation);
			news.setFullText(newsVO.getFullText());
			if (newsVO.getModificationDate() == null) {
				news.setModificationDate(news.getCreationDate());
			} else {
				news.setModificationDate(newsVO.getModificationDate());
			}
			if (newsVO.getNewsId() != null) {
				news.setId(newsVO.getNewsId());
			}
			news.setShortText(newsVO.getShortText());
			news.setTitle(newsVO.getTitle());
		} catch (ParseException ex) {
			throw new ServiceException(ex);
		}
		return news;
	}
}
