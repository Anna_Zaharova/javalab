package com.epam.newsmanagement.vo;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewsVO {
	@Size(min = 1, max = 100)
	@NotNull
	private String shortText;
	@Size(min = 1, max = 2000)
	@NotNull
	private String fullText;
	@Size(min = 1, max = 30)
	@NotNull
	private String title;
	@NotNull
	private String creationDate;
	@NotNull
	private Long authorId;
	private String locale;
	private Date modificationDate;
	private List<Long> tags;
	private Long newsId;

	public List<Long> getTags() {
		return tags;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public void setTags(List<Long> tags) {
		this.tags = tags;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
}
