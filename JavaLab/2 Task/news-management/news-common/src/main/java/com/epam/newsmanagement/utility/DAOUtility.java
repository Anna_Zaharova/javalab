package com.epam.newsmanagement.utility;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.exception.DAOException;

public final class DAOUtility {
	private DAOUtility() {
	}

	public static void closeResources(DataSource dataSource,
			Connection connection, Statement preparedStatement)
			throws DAOException {
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				DataSourceUtils.releaseConnection(connection, dataSource);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		}
	}

	public static void closeResources(DataSource dataSource,
			Connection connection, Statement statement, ResultSet resultSet)
			throws DAOException {
		try {
			if (statement != null) {
				statement.close();
			}
			if (resultSet != null) {
				resultSet.close();
			}
			if (connection != null) {
				DataSourceUtils.releaseConnection(connection, dataSource);
			}

		} catch (SQLException ex) {
			throw new DAOException(ex);
		}

	}
}
