package com.epam.newsmanagement.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Author extends Entity {

	private static final long serialVersionUID = -8401728072684639365L;
	@Size(min = 1, max = 30)
	@NotNull
	private String name;
	private Date expired;

	public Author() {

	}

	public Author(Long id, Date date) {
		super(id);
		this.expired = date;
	}

	public Author(Long id, String name) {
		super(id);
		this.name = name;
	}

	public Author(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public String toString() {
		return super.toString() + "\nname = " + name;
	}

	@Override
	public int hashCode() {
		int hash = 11, result = 0;
		int superHash = super.hashCode();
		result = hash * superHash
				+ ((expired == null) ? 0 : expired.hashCode());
		result = hash * superHash + result
				+ ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
