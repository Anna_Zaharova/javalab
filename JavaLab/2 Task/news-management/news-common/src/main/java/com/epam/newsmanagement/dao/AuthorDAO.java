package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Author;

/**
 * Provides extra operations for manipulate author data from the data source
 * 
 *
 */
public interface AuthorDAO extends AbstractDAO<Author, Long> {
	/**
	 * Returns the author by news identifier
	 * 
	 * @param newsID
	 *            news identifier
	 * @return the author which was found
	 * @throws DAOException
	 */
	Author fetchByNews(Long newsID) throws DAOException;

	/**
	 * Sets an author expired value
	 * 
	 * @param author
	 *            an author which will be update
	 * @throws DAOException
	 */
	void setExpired(Author author) throws DAOException;

	/**
	 * Returns authors which not expired
	 * 
	 * @return list of authors
	 * @throws DAOException
	 */

	List<Author> fetchNotExpired() throws DAOException;
}
