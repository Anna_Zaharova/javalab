package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.vo.Filter;

/**
 * Provides extra operations for manipulate News data from the data source
 * 
 *
 */
public interface NewsDAO extends AbstractDAO<News, Long> {

	/**
	 * Adds new tag to news
	 * 
	 * @param newsId
	 *            news identifier
	 * @param tagId
	 *            tag identifier
	 * @throws DAOException
	 */
	void addTag(Long newsId, Long tagId) throws DAOException;

	/**
	 * Adds new author to news
	 * 
	 * @param newsId
	 *            news identifier
	 * @param authorId
	 *            author identifier
	 * @throws DAOException
	 */
	void addAuthor(Long newsId, Long authorId) throws DAOException;

	/**
	 * Returns list of news by filter
	 * 
	 * @param firstNews
	 * @param lastNews
	 * @return
	 * @throws DAOException
	 */
	List<News> search(Filter filter, int firstNews, int lastNews)
			throws DAOException;

	/**
	 * Returns total news number satisfied the requirements of filter
	 * 
	 * @param filter
	 *            requirements that must be met by the news
	 * @return total news number
	 * @throws DAOException
	 */
	int fetchTotalNews(Filter filter) throws DAOException;

	/**
	 * Deletes news by news identifiers
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws DAOException
	 */
	void delete(List<Long> newsId) throws DAOException;

	/**
	 * Deletes all tags from news by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @throws DAOException
	 */

	void deleteTagByNewsId(Long newsId) throws DAOException;

	/**
	 * Deletes tag from all news by tag identifier
	 * 
	 * @param tagId
	 *            tag identifier
	 * @throws DAOException
	 */
	void deleteTagByTagId(Long tagId) throws DAOException;

	/**
	 * Deletes tags from all news listed in newsId
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws DAOException
	 */
	void deleteTags(List<Long> newsId) throws DAOException;

	/**
	 * Deletes authors from all news listed in newsId
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws DAOException
	 */
	void deleteAuthor(List<Long> newsId) throws DAOException;

	/**
	 * Deletes the author from news by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @throws DAOException
	 */
	void deleteAuthor(Long newsId) throws DAOException;

	/**
	 * Adds list of tags to news
	 * 
	 * @param newsId
	 *            news identifier
	 * @param tags
	 *            list of tags identifiers
	 * @throws DAOException
	 */
	void addTags(Long newsId, List<Long> tags) throws DAOException;

	/**
	 * Sets new author to the news
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param authorId
	 *            the author identifier
	 * @throws ServiceException
	 */
	void updateAuthor(Long newsId, Long authorId) throws DAOException;

	int fetchPosition(Long newsId) throws DAOException;
}
