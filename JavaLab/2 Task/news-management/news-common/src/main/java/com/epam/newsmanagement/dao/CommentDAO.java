package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Comment;

/**
 * 
 * Provides extra operations for manipulate comments data from the data source
 *
 */
public interface CommentDAO extends AbstractDAO<Comment, Long> {

	/**
	 * Receives the list of comments of certain news
	 * 
	 * @param newsId
	 *            news identifier
	 * @return the list of comments
	 * @throws DAOException
	 */
	List<Comment> fetchByNews(Long newsId) throws DAOException;

	/**
	 * Deletes all news comments by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @throws DAOException
	 */

	void deleteByNews(Long newsId) throws DAOException;

	/**
	 * Deletes all comments from each news
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws DAOException
	 */
	void deleteByNews(List<Long> newsId) throws DAOException;
}
