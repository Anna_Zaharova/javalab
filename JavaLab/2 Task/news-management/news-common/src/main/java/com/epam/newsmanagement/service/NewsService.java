package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.vo.Filter;

public interface NewsService {
	/**
	 * Edits information about the news
	 * 
	 * @param news
	 *            news to edit
	 * @throws ServiceException
	 */
	void update(News news) throws ServiceException;

	/**
	 * Returns the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the news
	 * @throws ServiceException
	 */
	News get(Long newsId) throws ServiceException;

	/**
	 * Deletes the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @throws ServiceException
	 */
	void delete(Long newsId) throws ServiceException;

	/**
	 * Deletes news by identifiers
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws ServiceException
	 */
	void delete(List<Long> newsId) throws ServiceException;

	/**
	 * Adds the news to the data source
	 * 
	 * @param news
	 *            news to add
	 * @throws ServiceException
	 */
	void add(News news) throws ServiceException;

	/**
	 * Adds new tag to the news
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param tagId
	 *            the tag identifier
	 * @throws ServiceException
	 */
	void addTag(Long newsId, Long tagId) throws ServiceException;

	/**
	 * Adds new author to the news
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param authorId
	 *            the author identifier
	 * @throws ServiceException
	 */
	void addAuthor(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Returns all news from data source
	 * 
	 * @return the list of news
	 * @throws ServiceException
	 */
	List<News> getAll() throws ServiceException;

	/**
	 * Returns news list satisfied the requirements of the filter
	 * 
	 * @param filter
	 *            requirements that must be met by each news
	 * @param firstNews
	 *            number of first news
	 * @param lastNews
	 *            number of the last news
	 * @return news list
	 * @throws ServiceException
	 */
	List<News> search(Filter filter, int firstNews, int lastNews)
			throws ServiceException;

	/**
	 * Returns total news number
	 * 
	 * @param filter
	 *            requirements that must be met by each news on the page
	 * @return total news number
	 * @throws ServiceException
	 */
	int getNewsCount(Filter filter) throws ServiceException;

	/**
	 * Deletes the author from news by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @throws ServiceException
	 */
	void deleteAuthor(Long authorId) throws ServiceException;

	/**
	 * Deletes authors from all news listed in newsId
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws ServiceException
	 */
	void deleteAuthor(List<Long> newsId) throws ServiceException;

	/**
	 * Deletes tags from all news listed in newsId
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws ServiceException
	 */
	void deleteTag(List<Long> newsId) throws ServiceException;

	/**
	 * Deletes all tags from news by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @throws ServiceException
	 */
	void deleteTagByNews(Long newsId) throws ServiceException;

	/**
	 * Deletes tag from all news by tag identifier
	 * 
	 * @param tagId
	 *            tag identifier
	 * @throws ServiceException
	 */
	void deleteTag(Long tagId) throws ServiceException;

	/**
	 * Adds list of tags to the news
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param tags
	 *            list of tags identifiers
	 * @throws ServiceException
	 */
	void addTags(Long newsId, List<Long> tags) throws ServiceException;

	/**
	 * Sets new author to the news
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param authorId
	 *            the author identifier
	 * @throws ServiceException
	 */
	void updateAuthorByNews(Long newsId, Long authorId) throws ServiceException;

	int getPosition(Long newsId) throws ServiceException;
}
