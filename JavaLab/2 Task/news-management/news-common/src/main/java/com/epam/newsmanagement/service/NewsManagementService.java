package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.ComplexNews;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.vo.Filter;
import com.epam.newsmanagement.vo.NewsVO;

public interface NewsManagementService {

	/**
	 * Returns the complex news by identifier
	 * 
	 * @param newsId
	 *            the complex news identifier
	 * @return the complex news
	 * @throws ServiceException
	 */
	public ComplexNews getComplexNews(Long newsId) throws ServiceException;

	/**
	 * Adds news with information about author and tags
	 * 
	 * @param newsVO
	 *            contains info about news, author and tags
	 * @return identifier of added news
	 * @throws ServiceException
	 */
	Long addNewsWithAuthorAndTags(NewsVO newsVO) throws ServiceException;

	/**
	 * Returns total news number
	 * 
	 * @param filter
	 *            requirements that must be met by each news on the page
	 * @return total news number
	 * @throws ServiceException
	 */
	int getNewsListByFilterCount(Filter filter) throws ServiceException;

	/**
	 * Updates all information about news include author and tags
	 * 
	 * @param newsVO
	 *            contains info about news
	 * @throws ServiceException
	 */
	void updateComplexNews(NewsVO newsVO) throws ServiceException;

	/**
	 * Returns all existing authors
	 * 
	 * @return authors list
	 * @throws ServiceException
	 */
	List<Author> getAllAuthors() throws ServiceException;

	/**
	 * Returns all existing tags
	 * 
	 * @return tags list
	 * @throws ServiceException
	 */
	List<Tag> getAllTags() throws ServiceException;

	/**
	 * Returns authors not expired
	 * 
	 * @return authors list
	 * @throws ServiceException
	 */
	List<Author> getAuthorsNotExpired() throws ServiceException;

	/**
	 * Adds author to the data source
	 * 
	 * @param author
	 *            author to add
	 * @throws ServiceException
	 */
	void addAuthor(Author author) throws ServiceException;

	/**
	 * Edits the information about the author
	 * 
	 * @param author
	 *            author to edit
	 * @throws ServiceException
	 */
	void updateAuthor(Author author) throws ServiceException;

	/**
	 * Sets expired value to the author
	 * 
	 * @param author
	 *            the author
	 * @throws ServiceException
	 */
	void setExpired(Author author) throws ServiceException;

	/**
	 * Edits information about the tag
	 * 
	 * @param tag
	 *            tag to edit
	 * @throws ServiceException
	 */
	void updateTag(Tag tag) throws ServiceException;

	/**
	 * Adds the tag to the data source
	 * 
	 * @param tag
	 *            tag to add
	 * @throws ServiceException
	 */
	void addTag(Tag tag) throws ServiceException;

	/**
	 * Deletes the tag by identifier
	 * 
	 * @param tagId
	 *            the tag identifier
	 * @throws ServiceException
	 */
	void deleteTagById(Long tagId) throws ServiceException;

	/**
	 * Deletes the comment by identifier
	 * 
	 * @param commentId
	 *            comment identifier
	 * @throws ServiceException
	 */
	void deleteCommentById(Long commentId) throws ServiceException;

	/**
	 * Adds the comment information
	 * 
	 * @param comment
	 *            the comment to add
	 * @throws ServiceException
	 */
	void addComment(Comment comment) throws ServiceException;

	/**
	 * Returns news list with author, tags and comments
	 * 
	 * @param filter
	 *            requirements that must be met by each news on the page
	 * @param start
	 *            the number of the first news on the page
	 * @param end
	 *            the number of the last news on the page
	 * @return list of news
	 * @throws ServiceException
	 */
	List<ComplexNews> getNewsListByFilter(Filter filter, int start, int end)
			throws ServiceException;

	/**
	 * Deletes all information about each news by news identifier
	 * 
	 * @param newsId
	 *            list of news identifiers
	 * @throws ServiceException
	 */
	void deleteCompleNewsListById(List<Long> newsId) throws ServiceException;

	int getPosition(Long newsId) throws ServiceException;;
}
