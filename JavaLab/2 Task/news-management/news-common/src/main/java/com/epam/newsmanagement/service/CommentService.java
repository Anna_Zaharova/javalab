package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface CommentService {

	/**
	 * Adds the comment information
	 * 
	 * @param comment
	 *            the comment to add
	 * @throws ServiceException
	 */
	void add(Comment comment) throws ServiceException;

	/**
	 * Deletes the comment by identifier
	 * 
	 * @param commentId
	 *            comment identifier
	 * @throws ServiceException
	 */
	void delete(Long commentId) throws ServiceException;

	/**
	 * Receives the list of comments by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @return the list of comments
	 * @throws ServiceException
	 */
	List<Comment> getByNews(Long newsId) throws ServiceException;

	/**
	 * Deletes all news comments by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @throws ServiceException
	 */

	void deleteByNews(Long newsId) throws ServiceException;

	/**
	 * Deletes all comments from news by identifiers
	 * 
	 * @param newsId
	 *            news identifiers
	 * @throws ServiceException
	 */
	void deleteByNews(List<Long> newsId) throws ServiceException;
}
