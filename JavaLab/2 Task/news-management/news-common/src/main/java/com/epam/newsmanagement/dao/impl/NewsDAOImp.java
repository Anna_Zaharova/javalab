package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.utility.DAOUtility;
import com.epam.newsmanagement.vo.Filter;

@Component
public class NewsDAOImp implements NewsDAO {
	private final static String SQL_READ_ALL_NEWS = "SELECT news_id, short_text, full_text, title, creation_date, modification_date FROM News ORDER BY modification_date";
	private final static String SQL_ADD_NEWS_TAG = "INSERT INTO News_Tag(news_id, tag_id) VALUES (?, ?)";
	private final static String SQL_ADD_NEWS_AUTHOR = "INSERT INTO News_Author(news_id, author_id) VALUES(?, ?)";
	private final static String SQL_INSERT_NEWS = "INSERT INTO News(news_id, short_text, full_text, title, creation_date, "
			+ "modification_date) VALUES (news_seq.nextval, ?, ?, ?, ?, ?)";
	private final static String SQL_DELETE_NEWS = "DELETE FROM News WHERE news_id =?";
	private final static String SQL_DELETE_NEWS_LIST = "DELETE FROM News WHERE news_id IN (idList)";
	private final static String SQL_DELETE_NEWS_TAGS_BY_NEWS_ID = "DELETE FROM News_Tag WHERE news_id=?";
	private final static String SQL_DELETE_NEWS_TAGS_BY_NEWS_LIST = "DELETE FROM News_tag WHERE news_id IN (idList)";
	private final static String SQL_DELETE_NEWS_TAGS_BY_TAG_ID = "DELETE FROM News_Tag WHERE tag_id=?";
	private final static String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM News_Author WHERE news_id=?";
	private final static String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_LIST = "DELETE FROM News_Author WHERE news_id IN (idList)";
	private final static String SQL_READ_NEWS = "SELECT news_id, short_text, full_text, title, creation_date, modification_date"
			+ " FROM News WHERE news_id = ?";
	private final static String SQL_UPDATE_NEWS = "UPDATE News SET short_text = ?, full_text = ?, title = ?, "
			+ "modification_date = ? WHERE news_id = ?";
	private final static String SQL_UPDATE_NEWS_AUTHOR = "UPDATE News_Author SET author_id = ? WHERE news_id = ?";
	private static String SQL_READ_NEWS_BY_AUTHOR_AND_TAGS = "SELECT * FROM(SELECT  N.news_id, N.short_text, N.full_text, N.title, N.creation_date,"
			+ " N.modification_date, coalesce(C.comments_amount, 0) comments_amount, row_number() OVER ( ORDER BY C.comments_amount DESC NULLS LAST, N.modification_date DESC)"
			+ " RN FROM News N LEFT OUTER JOIN (SELECT news_id , COUNT(*) as comments_amount FROM Comments GROUP BY news_id) C ON N.news_id = C.news_id ";
	private static final String SQL_END = ")WHERE RN BETWEEN ? AND ? ";
	private static final String BY_AUTHOR = " INNER JOIN News_Author A ON A.news_id = N.news_id AND A.author_id=?";
	private static final String BY_TAGS = " INNER JOIN News_Tag T ON T.news_id = N.news_id AND T.tag_id IN (idList)";
	private static final String SQL_READ_TOTAL_NEWS = "SELECT count(*) total FROM News N ";
	private static final String SQL_GET_POSITION = "SELECT RN as position FROM (SELECT N.news_id, row_number() OVER ( ORDER BY C.comments_amount DESC NULLS LAST, N.modification_date DESC)"
			+ " RN FROM News N LEFT OUTER JOIN (SELECT news_id , COUNT(*) as comments_amount FROM Comments GROUP BY news_id) C ON N.news_id = C.news_id) WHERE news_id = ?";
	@Autowired
	private DataSource dataSource;

	@Override
	public Long add(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		long id = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS,
					new String[] { "news_id" });
			prepareStatementForCreate(preparedStatement, news);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the news not created");
			}
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
				news.setId(id);
			} else {
				throw new DAOException("the key not generated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return id;
	}

	@Override
	public News fetch(Long id) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_NEWS);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createNews(resultSet);
			} else {
				throw new DAOException(String.format(
						"the news with id = %d not found", id));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public void update(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			prepareStatementForUpdate(preparedStatement, news);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(String.format(
						"the news with id = %d not updated", news.getId()));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(String.format(
						"the news with id = %d not deleted", id));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(List<Long> newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(generateQueryWithTags(
							SQL_DELETE_NEWS_LIST, newsId.size()));
			for (int i = 0; i < newsId.size(); i++) {
				preparedStatement.setLong((i + 1), newsId.get(i));
			}
			if (preparedStatement.executeUpdate() < newsId.size()) {
				throw new DAOException(String.format(
						"news with id = %s not deleted", newsId));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void deleteTagByNewsId(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_NEWS_TAGS_BY_NEWS_ID);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() < 1) {
				throw new DAOException(String.format(
						"the tag of the news with id = %d not deleted", id));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void deleteTagByTagId(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_NEWS_TAGS_BY_TAG_ID);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() < 1) {
				throw new DAOException(String.format(
						"the news with tag id = %d not deleted", id));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void deleteTags(List<Long> newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(generateQueryWithTags(
							SQL_DELETE_NEWS_TAGS_BY_NEWS_LIST, newsId.size()));
			for (int i = 0; i < newsId.size(); i++) {
				preparedStatement.setLong((i + 1), newsId.get(i));
			}
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void deleteAuthor(List<Long> newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(generateQueryWithTags(
							SQL_DELETE_NEWS_AUTHOR_BY_NEWS_LIST, newsId.size()));
			for (int i = 0; i < newsId.size(); i++) {
				preparedStatement.setLong((i + 1), newsId.get(i));
			}
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void deleteAuthor(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_NEWS_AUTHOR);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() < 1) {
				throw new DAOException(String.format(
						"the author of the news with id = %d not deleted", id));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void updateAuthor(Long newsId, Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_UPDATE_NEWS_AUTHOR);
			preparedStatement.setLong(1, authorId);
			preparedStatement.setLong(2, newsId);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(
						String.format(
								"the author with id = %d of the news with id = %d is not updated",
								authorId, newsId));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<News> fetchAll() throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_ALL_NEWS);
			List<News> news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createNews(resultSet));
			}
			return news;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection, statement,
					resultSet);
		}
	}

	@Override
	public void addTag(Long newsId, Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_TAG);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(
						String.format(
								"the tag with id = %d not added to the news with id = %d",
								tagId, newsId));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void addTags(Long newsId, List<Long> tags) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_TAG);
			for (Long id : tags) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void addAuthor(Long newsId, Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_ADD_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException(
						String.format(
								"the author with id = %d not added to the news with id = %d",
								authorId, newsId));
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<News> search(Filter filter, int firstNews, int lastNews)
			throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		List<Long> tags = filter.getTags();
		Long authorId = filter.getAuthor();
		try {
			int i = 1;
			connection = DataSourceUtils.getConnection(dataSource);
			String query = generateQueryWithAuthorAndTags(filter);
			preparedStatement = connection.prepareStatement(query);
			if (authorId != null) {
				preparedStatement.setLong(i++, authorId);
			}
			if (tags != null) {
				for (int k = 0; k < tags.size(); k++) {
					preparedStatement.setLong(i++, tags.get(k));
				}
			}
			preparedStatement.setInt(i++, firstNews);
			preparedStatement.setInt(i++, lastNews);
			resultSet = preparedStatement.executeQuery();
			List<News> news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createNews(resultSet));
			}
			return news;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public int fetchTotalNews(Filter filter) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			int i = 1;
			connection = DataSourceUtils.getConnection(dataSource);
			String query = generateQueryTotalNews(filter);
			preparedStatement = connection.prepareStatement(query);
			if (filter.getAuthor() != null) {
				preparedStatement.setLong(i++, filter.getAuthor());
			}
			List<Long> tags = filter.getTags();
			if (tags != null) {
				for (int k = 0; k < tags.size(); k++) {
					preparedStatement.setLong(i++, tags.get(k));
				}
			}
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt("total");
			} else
				throw new DAOException();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public int fetchPosition(Long newsId) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_POSITION);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt("position");
			} else
				throw new DAOException();
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOUtility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	private String generateQueryWithAuthorAndTags(Filter filter) {
		StringBuilder sb = new StringBuilder(SQL_READ_NEWS_BY_AUTHOR_AND_TAGS);
		addQueryClause(sb, filter);
		sb.append(SQL_END);
		return sb.toString();
	}

	private String generateQueryTotalNews(Filter filter) {
		StringBuilder sb = new StringBuilder(SQL_READ_TOTAL_NEWS);
		addQueryClause(sb, filter);
		return sb.toString();
	}

	private void addQueryClause(StringBuilder sb, Filter filter) {
		if (filter.getAuthor() != null) {
			sb.append(BY_AUTHOR);
		}
		if (filter.getTags() != null && !filter.getTags().isEmpty()) {
			sb.append(generateQueryWithTags(BY_TAGS, filter.getTags().size()));
		}
	}

	private String generateQueryWithTags(String query, int length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			sb.append("?");
			if (i != length - 1) {
				sb.append(",");
			}
		}
		return query.replace("idList", sb.toString());
	}

	private void prepareStatementForCreate(PreparedStatement preparedStatement,
			News news) throws SQLException {
		preparedStatement.setString(1, news.getShortText());
		preparedStatement.setString(2, news.getFullText());
		preparedStatement.setString(3, news.getTitle());
		preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate()
				.getTime()));
		preparedStatement.setDate(5, new java.sql.Date(news
				.getModificationDate().getTime()));
	}

	private News createNews(ResultSet resultSet) throws SQLException {
		News news = new News();
		news.setCreationDate(resultSet.getTimestamp("creation_date"));
		news.setFullText(resultSet.getString("full_text"));
		news.setId(resultSet.getLong("news_id"));
		news.setModificationDate(new Date(resultSet
				.getDate("modification_date").getTime()));
		news.setShortText(resultSet.getString("short_text"));
		news.setTitle(resultSet.getString("title"));
		return news;
	}

	private void prepareStatementForUpdate(PreparedStatement preparedStatement,
			News news) throws SQLException {
		preparedStatement.setString(1, news.getShortText());
		preparedStatement.setString(2, news.getFullText());
		preparedStatement.setString(3, news.getTitle());
		preparedStatement.setDate(4, new java.sql.Date(news
				.getModificationDate().getTime()));
		preparedStatement.setLong(5, news.getId());
	}
}