package com.epam.newsmanagement.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DateConversionUtility {
	private static final Map<String, String> localeFormatMap;
	static {
		Map<String, String> map = new HashMap<>(2);
		map.put("ru", "dd.MM.yy");
		map.put("en", "MM/dd/yy");
		localeFormatMap = Collections.unmodifiableMap(map);
	}

	public static Date convertFromStringToDate(String locale, String dateString)
			throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(
				localeFormatMap.get(locale), Locale.ENGLISH);
		return dateFormat.parse(dateString);
	}

	public static String convertFromDateToString(String locale, Date date) {
		DateFormat dateFormat = new SimpleDateFormat(
				localeFormatMap.get(locale), Locale.ENGLISH);
		return dateFormat.format(date);
	}
}
