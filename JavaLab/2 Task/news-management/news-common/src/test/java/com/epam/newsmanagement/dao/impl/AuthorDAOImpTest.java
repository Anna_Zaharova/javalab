package com.epam.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class AuthorDAOImpTest {
	@Autowired
	private AuthorDAO authorDAO;

	@Test
	public void insert() throws DAOException {
		Author author = new Author("Vatja");
		authorDAO.add(author);
		assertTrue(author.getId() > 0);
		author = authorDAO.fetch(author.getId());
		assertEquals(author.getName(), "Vatja");
	}

	@Test
	public void readById() throws DAOException {
		Author author = authorDAO.fetch(1l);
		assertTrue(author.getId() == 1l);
		assertEquals(author.getName(), "Petr");
	}

	@Test
	public void update() throws DAOException {
		Author author = new Author(1l, "Alex");
		authorDAO.update(author);
		author = authorDAO.fetch(1l);
		assertEquals(author.getName(), "Alex");
	}

	@Test(expected = DAOException.class)
	public void deleteSuccess() throws DAOException {
		try {
			authorDAO.delete(4l);
		} catch (DAOException ex) {
			fail();
		}
		authorDAO.fetch(4l);
	}

	@Test(expected = DAOException.class)
	public void deleteFail() throws DAOException {
		authorDAO.delete(5l);
	}

	@Test
	public void setExpired() throws DAOException {
		Author author = authorDAO.fetch(1l);
		Date date = new Date();
		author.setExpired(date);
		authorDAO.setExpired(author);
		assertEquals((authorDAO.fetch(1l)).getExpired().getTime(),
				date.getTime());
	}

	@Test
	public void readNewsAuthor() throws DAOException {
		Author author = authorDAO.fetchByNews(1l);
		assertEquals(author.getName(), "Helen");
		assertTrue(author.getId() == 2);
	}

	@Test
	public void readAll() throws DAOException {
		List<Author> authors = authorDAO.fetchAll();
		assertTrue(authors.size() == 4);
		Author author = authors.get(3);
		assertEquals(author.getName(), "Petr");
		assertTrue(author.getId() == 1l);
	}
}
