package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.ComplexNews;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceImpTest {

	@Mock
	private NewsService newsService;
	@Mock
	private CommentService commentService;
	@Mock
	private TagService tagService;
	@Mock
	private AuthorService authorService;
	private NewsManagementService newsManagementService;
	private ComplexNews complexNews;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		newsManagementService = new NewsManagementServiceImp(newsService,
				commentService, tagService, authorService);
		initComplexNews();
	}

	private List<Tag> createTagList() {
		List<Tag> tags = new ArrayList<>();
		tags.add(new Tag(1l, "tag1"));
		tags.add(new Tag(2l, "tag2"));
		tags.add(new Tag(3l, "tag3"));
		return tags;
	}

	private List<Comment> createCommentsList() {
		List<Comment> comments = new ArrayList<>();
		for (int i = 1; i < 5; i++) {
			Comment comment = new Comment();
			comment.setId((long) i);
			comment.setCommentText("some_comment" + i);
			comment.setCreationDate(new Date());
			comment.setNewsId((long) i);
			comments.add(comment);
		}
		return comments;
	}

	private News createNews() {
		News news = new News();
		news.setCreationDate(new Date());
		news.setFullText("full_text");
		news.setId(1l);
		news.setModificationDate(new Date());
		news.setTitle("title");
		news.setShortText("short_text");
		return news;
	}

	private List<News> createNewsList() {
		List<News> news = new ArrayList<News>();
		news.add(createNews());
		return news;
	}

	private void initComplexNews() {
		complexNews = new ComplexNews();
		complexNews.setAuthor(new Author(1l, "Vlad"));
		complexNews.setTags(createTagList());
		complexNews.setComments(createCommentsList());
		complexNews.setNews(createNews());
	}

	@Test
	public void deleteCommentById() throws ServiceException {
		newsManagementService.deleteCommentById(1l);
		verify(commentService).delete(1l);
		verifyNoMoreInteractions(commentService);
	}

	@Test
	public void addComment() throws ServiceException {
		Comment comment = new Comment();
		newsManagementService.addComment(comment);
		verify(commentService).add(comment);
		verifyNoMoreInteractions(commentService);
	}

	@Test
	public void deleteTagById() throws ServiceException {
		when(tagService.isUsed(1l)).thenReturn(false);
		newsManagementService.deleteTagById(1l);
		verify(tagService).isUsed(1l);
		verify(tagService).delete(1l);
	}
}
