package com.epam.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class CommentDAOImpTest {
	@Autowired
	private CommentDAO commentDAO;

	@Test
	public void create() throws DAOException {
		Date date = new Date();
		Comment comment = new Comment(1l, "hello", date);
		commentDAO.add(comment);
		assertTrue(comment.getId() > 0);
		comment = commentDAO.fetch(comment.getId());
		assertEquals(comment.getCommentText(), "hello");
		assertEquals(comment.getCreationDate().getTime(), date.getTime());
		assertTrue(comment.getNewsId() == 1l);
	}

	@Test
	public void update() throws DAOException {
		Comment comment = commentDAO.fetch(2l);
		comment.setCommentText("someText");
		commentDAO.update(comment);
		comment = commentDAO.fetch(2l);
		assertEquals("someText", comment.getCommentText());
		assertTrue(comment.getId() == 2l);
	}

	@Test(expected = DAOException.class)
	public void deleteSuccess() throws DAOException {
		try {
			commentDAO.delete(2l);
		} catch (DAOException ex) {
			fail();
		}
		commentDAO.fetch(2l);
	}

	@Test(expected = DAOException.class)
	public void deleteFail() throws DAOException {
		commentDAO.delete(10l);
	}

	@Test
	public void read() throws DAOException {
		Comment comment = commentDAO.fetch(1l);
		assertEquals("Cool!", comment.getCommentText());
		assertTrue(comment.getId() == 1l);
	}

	@Test
	public void readNewsComment() throws DAOException {
		List<Comment> comments = commentDAO.fetchByNews(2l);
		assertTrue(comments.size() == 2);
		Comment comment = comments.get(1);
		assertTrue(comment.getId() == 2l);
		assertEquals(comment.getCommentText(), "Interesting");
	}

	@Test
	public void deleteNewsComments() throws DAOException {
		try {
			commentDAO.deleteByNews(1l);
		} catch (DAOException ex) {
			fail();
		}
		List<Comment> comments = commentDAO.fetchByNews(1l);
		assertTrue(comments.size() == 0);
	}

	@Test
	public void readAll() throws DAOException {
		List<Comment> comments = commentDAO.fetchAll();
		assertTrue(comments.size() == 4);
		Comment comment = comments.get(3);
		assertEquals(comment.getCommentText(), "Interesting");
		assertTrue(comment.getId() == 2l);
		assertTrue(comment.getNewsId() == 2l);
	}
}
