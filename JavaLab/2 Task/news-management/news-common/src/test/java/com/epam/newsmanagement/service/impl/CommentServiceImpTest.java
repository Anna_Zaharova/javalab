package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.CommentService;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImpTest {
	@Mock
	private CommentDAO commentDAO;
	private CommentService commentService;
	private Comment comment;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		commentService = new CommentServiceImp(commentDAO);
		comment = new Comment("comment", new Date());
		comment.setId(1l);

	}

	@Test
	public void deleteCommentById() throws Exception {
		commentService.delete(comment.getId());
		verify(commentDAO).delete(comment.getId());
		verifyNoMoreInteractions(commentDAO);
	}

	@Test
	public void addComment() throws Exception {
		commentService.add(comment);
		verify(commentDAO).add(comment);
		verifyNoMoreInteractions(commentDAO);
	}

	@Test
	public void getNewsComments() throws Exception {
		List<Comment> comments = new ArrayList<Comment>() {
			{
				add(new Comment("tt", new Date()));
			}
		};
		when(commentDAO.fetchByNews(8l)).thenReturn(comments);
		List<Comment> commentList = commentService.getByNews(8l);
		assertTrue(comments.size() == commentList.size());
		verify(commentDAO).fetchByNews(8l);
		verifyNoMoreInteractions(commentDAO);
	}

	@Test
	public void deleteNewsComments() throws Exception {
		commentService.deleteByNews(1l);
		verify(commentDAO).deleteByNews(1l);
		verifyNoMoreInteractions(commentDAO);
	}

}
