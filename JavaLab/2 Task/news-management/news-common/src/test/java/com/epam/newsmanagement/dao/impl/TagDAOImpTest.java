package com.epam.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class TagDAOImpTest {
	@Autowired
	private TagDAO tagDAO;

	@Test
	public void readById() throws DAOException {
		Tag tag = tagDAO.fetch(3l);
		assertTrue(tag.getId() == 3l);
		assertEquals("Python", tag.getName());
	}

	@Test
	public void update() throws DAOException {
		Tag tag = tagDAO.fetch(1l);
		tag.setName("Test");
		tagDAO.update(tag);
		tag = tagDAO.fetch(1l);
		assertEquals("Test", tag.getName());
		assertTrue(tag.getId() == 1l);
	}

	@Test(expected = DAOException.class)
	public void deleteSuccess() throws DAOException {
		try {
			tagDAO.delete(5l);
		} catch (DAOException ex) {
			fail();
		}
		tagDAO.fetch(5l);
	}

	@Test(expected = DAOException.class)
	public void deleteFail() throws DAOException {
		tagDAO.delete(3l);
	}

	@Test
	public void create() throws DAOException {
		Tag tag = new Tag("OOP");
		Long id = tagDAO.add(tag);
		assertTrue(id > 0);
		tag = tagDAO.fetch(id);
		assertEquals(tag.getName(), "OOP");
		assertTrue(tag.getId() == id);
	}

	@Test
	public void readNewsTag() throws DAOException {
		List<Tag> tagList = tagDAO.fetchByNews(1l);
		assertTrue(tagList.size() == 2);
		Tag tag = tagList.get(1);
		assertEquals(tag.getName(), "Java");
		assertTrue(tag.getId() == 1l);
	}

	@Test
	public void readAll() throws DAOException {
		List<Tag> tagList = tagDAO.fetchAll();
		assertTrue(tagList.size() == 4);
		Tag tag = tagList.get(2);
		assertEquals(tag.getName(), "Java");
		assertTrue(tag.getId() == 1l);
	}
}
