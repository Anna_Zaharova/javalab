package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.vo.Filter;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImpTest {
	@Mock
	private NewsDAO newsDAO;
	private NewsService newsService;
	private News news;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		newsService = new NewsServiceImp(newsDAO);
		initNews();
	}

	private void initNews() {
		news = new News();
		news.setCreationDate(new Date());
		news.setFullText("full text");
		news.setId(12l);
		news.setShortText("short text");
		news.setTitle("title");
	}

	@Test
	public void addNews() throws Exception {
		newsService.add(news);
		verify(newsDAO).add(news);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void getNewsById() throws Exception {
		when(newsDAO.fetch(news.getId())).thenReturn(news);
		News ns = newsService.get(news.getId());
		assertEquals(news.getTitle(), ns.getTitle());
		assertTrue(news.getId() == ns.getId());
		verify(newsDAO).fetch(news.getId());
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void updateNews() throws Exception {
		newsService.update(news);
		verify(newsDAO).update(news);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void deleteNewsById() throws Exception {
		newsService.delete(news.getId());
		verify(newsDAO).delete(news.getId());
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void deleteNewsAuthor() throws Exception {
		newsService.deleteAuthor(1l);
		verify(newsDAO).deleteAuthor(1l);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void deleteNewsTagByNewsId() throws Exception {
		newsService.deleteTagByNews(1l);
		verify(newsDAO).deleteTagByNewsId(1l);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void deleteNewsTagByTagId() throws Exception {
		newsService.deleteTag(1l);
		verify(newsDAO).deleteTagByTagId(1l);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void addNewsTag() throws Exception {
		newsService.addTag(1l, 1l);
		verify(newsDAO).addTag(1l, 1l);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void addNewsAuthor() throws Exception {
		newsService.addAuthor(1l, 1l);
		verify(newsDAO).addAuthor(1l, 1l);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void getAll() throws Exception {
		List<News> newsList = new ArrayList<News>() {
			{
				add(news);
				add(new News());
			}
		};
		when(newsDAO.fetchAll()).thenReturn(newsList);
		List<News> ns = newsService.getAll();
		assertTrue(newsList.size() == ns.size());
		verify(newsDAO).fetchAll();
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void getNewsByFilter() throws Exception {
		Filter filter = new Filter();
		List<News> newsList = new ArrayList<News>() {
			{
				add(news);
				add(new News());
			}
		};
		when(newsDAO.search(filter, 1, 2)).thenReturn(newsList);
		List<News> ns = newsService.search(filter, 1, 2);
		assertTrue(newsList.size() == ns.size());
		verify(newsDAO).search(filter, 1, 2);
		verifyNoMoreInteractions(newsDAO);
	}

}
