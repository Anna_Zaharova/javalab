package com.epam.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.vo.Filter;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class NewsDAOImpTest {
	@Autowired
	private NewsDAO newsDAO;
	@Autowired
	private TagDAO tagDAO;

	@Test
	public void readById() throws DAOException {
		News news = newsDAO.fetch(1l);
		assertTrue(news.getId() == 1l);
		assertEquals(news.getTitle(), "title1");
		assertEquals(news.getCreationDate().toString(), "2015-03-02 12:10:10.0");
		assertEquals(news.getFullText(), "full_text1");
		assertEquals(news.getShortText(), "short_text1");
	}

	@Test
	public void create() throws DAOException {
		News news = createNews();
		Long id = newsDAO.add(news);
		news = newsDAO.fetch(id);
		assertEquals(news.getTitle(), "test title");
		assertEquals(news.getFullText(), "test full text");
		assertEquals(news.getShortText(), "test short text");
	}

	private News createNews() {
		News news = new News();
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		news.setFullText("test full text");
		news.setShortText("test short text");
		news.setTitle("test title");
		return news;
	}

	@Test
	public void update() throws DAOException {
		News news = newsDAO.fetch(2l);
		String newTitle = "new title from update test";
		news.setTitle(newTitle);
		news.setModificationDate(new Date());
		newsDAO.update(news);
		news = newsDAO.fetch(2l);
		assertEquals(newTitle, news.getTitle());
	}

	@Test(expected = DAOException.class)
	public void deleteSuccess() throws DAOException {
		newsDAO.delete(2l);

	}

	@Test
	public void deleteFail() throws DAOException {
		try {
			newsDAO.delete(4l);
		} catch (DAOException ex) {
			fail();
		}
	}

	@Test
	public void readAll() throws DAOException {
		List<News> newsList = newsDAO.fetchAll();
		assertEquals(newsList.size(), 4);
		News news = newsList.get(3);
		assertTrue(news.getId() == 3l);
		assertEquals(news.getTitle(), "title3");
		assertEquals(news.getShortText(), "short_text3");
	}

	@Test
	public void deleteNewsTag() throws DAOException {
		newsDAO.deleteTagByNewsId(2l);
	}

	@Test
	public void deleteNewsAuthor() throws DAOException {
		newsDAO.deleteAuthor(2l);
	}

	@Test
	public void addNewsTag() throws DAOException {
		newsDAO.addTag(3l, 2l);
		List<Tag> tagList = tagDAO.fetchByNews(3l);
		assertTrue(tagList.size() == 2);
		Tag tag = tagList.get(1);
		assertEquals(tag.getName(), "Python");
		assertTrue(tag.getId() == 3l);
	}

	@Test
	public void addNewsAuthor() throws DAOException {
		newsDAO.addAuthor(3l, 1l);
	}

	@Test
	public void readNewsWithEmptyFilter() throws DAOException {
		Filter filter = new Filter();
		List<News> news = newsDAO.search(filter, 1, 5);
		assertTrue(news.size() == 4);
	}

	@Test
	public void readNewsWithAuthor() throws DAOException {
		Filter filter = new Filter();
		filter.setAuthor(3l);
		List<News> news = newsDAO.search(filter, 1, 5);
		assertTrue(news.size() == 1);
		assertTrue(news.get(0).getId() == 2l);
	}

	@Test
	public void readNewsWithTag() throws DAOException {
		Filter filter = new Filter();
		List<Long> tags = new ArrayList<>();
		tags.add(2l);
		filter.setTags(tags);
		List<News> news = newsDAO.search(filter, 1, 5);
		assertTrue(news.size() == 2);
		assertTrue(news.get(0).getId() == 2l);
		assertTrue(news.get(1).getId() == 1l);
	}

	@Test
	public void readNewsWithTagAndAuthor() throws DAOException {
		Filter filter = new Filter();
		filter.setAuthor(2l);
		List<Long> tags = new ArrayList<>();
		tags.add(2l);
		tags.add(3l);
		filter.setTags(tags);
		List<News> news = newsDAO.search(filter, 1, 5);
		assertTrue(news.size() == 1);
		assertTrue(news.get(0).getId() == 1l);
	}

	@Test
	public void readTotalNews() throws DAOException {
		assertTrue(newsDAO.fetchTotalNews(new Filter()) == 4);
	}
}
