package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.AuthorService;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImpTest {

	private AuthorService authorService;
	@Mock
	private AuthorDAO authorDAO;
	private Author author;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		authorService = new AuthorServiceImp(authorDAO);
		author = new Author(1l, "Java");
	}

	@Test
	public void getAuthorById() throws Exception {
		when(authorDAO.fetch(1l)).thenReturn(author);
		Author nAuthor = authorService.get(1l);
		assertEquals("Java", nAuthor.getName());
		assertTrue(1l == nAuthor.getId());
		verify(authorDAO).fetch(1l);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void updateAuthor() throws Exception {
		authorService.update(author);
		verify(authorDAO).update(author);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void deleteAuthorById() throws Exception {
		authorService.delete(author.getId());
		verify(authorDAO).delete(author.getId());
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void addAuthor() throws Exception {
		authorService.add(author);
		verify(authorDAO).add(author);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void getNewsAuthor() throws Exception {
		when(authorDAO.fetchByNews(4l)).thenReturn(author);
		Author nAuthor = authorService.getByNews(4l);
		assertEquals("Java", nAuthor.getName());
		assertTrue(1l == nAuthor.getId());
		verify(authorDAO).fetchByNews(4l);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void setExpired() throws Exception {
		authorService.setExpired(author);
		verify(authorDAO).setExpired(author);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void getAllAuthors() throws Exception {
		List<Author> authors = new ArrayList<Author>() {
			{
				add(author);
				add(new Author(2l, "Tomas"));
			}
		};
		when(authorDAO.fetchAll()).thenReturn(authors);
		List<Author> authorlist = authorService.getAll();
		assertTrue(authors.size() == authorlist.size());
		verify(authorDAO).fetchAll();
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void getAuthorsNotExpired() throws Exception {
		List<Author> authors = new ArrayList<Author>() {
			{
				add(author);
				add(new Author(2l, "Tomas"));
			}
		};
		when(authorDAO.fetchNotExpired()).thenReturn(authors);
		List<Author> authorlist = authorService.getNotExpired();
		assertTrue(authors.size() == authorlist.size());
		verify(authorDAO).fetchNotExpired();
		verifyNoMoreInteractions(authorDAO);
	}
}
