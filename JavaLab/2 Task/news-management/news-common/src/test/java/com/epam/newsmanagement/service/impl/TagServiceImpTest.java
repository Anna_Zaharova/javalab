package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.TagService;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImpTest {
	@Mock
	private TagDAO tagDAO;
	private TagService tagService;
	private Tag tag;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		tagService = new TagServiceImp(tagDAO);
		tag = new Tag(1l, "Java");
	}

	@Test
	public void updateTag() throws Exception {
		tagService.update(tag);
		verify(tagDAO).update(tag);
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void deleteTagById() throws Exception {
		tagService.delete(tag.getId());
		verify(tagDAO).delete(tag.getId());
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void addTag() throws Exception {
		tagService.add(tag);
		verify(tagDAO).add(tag);
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void getNewsTags() throws Exception {
		List<Tag> tags = new ArrayList<Tag>() {
			{
				add(new Tag("as"));
				add(new Tag("asaa"));
			}
		};
		when(tagDAO.fetchByNews(1l)).thenReturn(tags);
		List<Tag> ts = tagService.getByNews(1l);
		assertTrue(ts.size() == tags.size());
		verify(tagDAO).fetchByNews(1l);
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void getAllTags() throws Exception {
		List<Tag> tags = new ArrayList<Tag>() {
			{
				add(new Tag());
				add(new Tag());
				add(new Tag());
			}
		};
		when(tagDAO.fetchAll()).thenReturn(tags);
		List<Tag> ts = tagService.getAll();
		assertTrue(ts.size() == tags.size());
		verify(tagDAO).fetchAll();
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void isUsed() throws Exception {
		when(tagDAO.isUsed(1l)).thenReturn(false);
		assertFalse(tagService.isUsed(1l));
		verify(tagDAO).isUsed(1l);
		verifyNoMoreInteractions(tagDAO);
	}
}
