<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:url value="/newsList" var="newsList" />
<c:url value="/reset" var="reset" />
<div class="news-list-admin">
	<form action="${reset}">
		<input type="submit" class="news-list reset-btn" id="reset"
			value="<spring:message code="button.reset"/>" />
	</form>
	<form:form action="${newsList}" modelAttribute="filter" method="GET">
		<input type="submit" class="news-list filter-btn" id="filter"
			value="<spring:message code="button.filter"/>" />
		<div id="dropdown" class="news-list">
			<div id="dropdown-tags">
				<select id="select-tags" size="1"><option><spring:message
							code="label.selectTag" /></option></select>
			</div>
			<div id="checkboxes">
				<div class="tag-list">
					<span><spring:message code="label.selectTag" /></span>
					<form:checkboxes items="${tagList}" path="tags" itemLabel="name"
						itemValue="id" />
				</div>
			</div>
		</div>
		<spring:message code="label.selectAuthor" var="selectAuthor" />
		<form:select path="author" cssClass="news-list">
			<form:option value="" label="${selectAuthor }" />
			<form:options items="${authorList}" itemValue="id" itemLabel="name" />
		</form:select>
	</form:form>
	<div class="clear"></div>
	<div class="search-result">
		<c:if test="${not empty newsListFilter}">
			<c:forEach items="${newsListFilter}" var="complexNews"
				varStatus="status">
				<div class="news-title">${complexNews.news.title}</div>
				<div class="news-author">(${complexNews.author.name})</div>
				<div class="news-create">
					<fmt:formatDate type="date" dateStyle="short"
						value="${complexNews.news.modificationDate}" />
				</div>
				<div class="clear news-short">${complexNews.news.shortText }</div>
				<a class="news-view"
					href="<c:url value="/viewNews/${(pageNumber-1)*newsPerPage+status.count}"/>"><spring:message
						code="button.view" /></a>
				<div class="news-comments">
					<spring:message code="label.comments" />
					(${fn:length(complexNews.comments)})
				</div>
				<div class="news-tags">
					<c:forEach items="${complexNews.tags}" var="tag" varStatus="loop">
					${tag.name}<c:if test="${!loop.last}">,</c:if>
					</c:forEach>
				</div>
				<div class="clear"></div>
			</c:forEach>
		</c:if>
	</div>
	<c:if test="${empty newsListFilter}">
		<div class="news-error">
			<spring:message code="error.newsNotFound" />
		</div>
	</c:if>
	<fmt:formatNumber
		value="${((newsCount - (newsCount-1)%newsPerPage+1)/newsPerPage)+0.5}"
		type="number" pattern="#" var="pageCount" />
	<c:if test="${pageCount > 1 && not empty newsListFilter}">
		<div class="pagination">
			<c:forEach begin="1" end="${pageCount}" varStatus="loop">
				<div class="pages">
					<div
						<c:if test="${loop.index == pageNumber}">
							class="page current-page"
							</c:if>
						<c:if test="${loop.index != pageNumber}">
							class="page"
							</c:if>>
						<a href="<c:url value="/newsList/${loop.index}"/>">${loop.index}</a>
					</div>
				</div>
			</c:forEach>
		</div>
	</c:if>
</div>