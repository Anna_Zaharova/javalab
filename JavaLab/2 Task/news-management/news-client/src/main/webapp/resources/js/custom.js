$(function() {
	$('#checkboxes span').click(function() {
		if (!$(event.target).is('#checkboxes [type=checkbox]')) {
			var checkbox = $(this).children();
			checkbox.prop("checked", !checkbox.prop("checked"));
		}
	});
	$('#checkboxes label').click(function() {
		$(this).prev().prop("checked", !$(this).prev().prop("checked"));
	});
	$('html').click(
			function() {
				if ((!$(event.target).is('#checkboxes input')
						&& !$(event.target).is('#checkboxes label') && !$(
						event.target).is('#checkboxes span'))
						&& $("#checkboxes").hasClass('open')) {
					$("#checkboxes").removeClass('open');
				} else if ($(event.target).is('#dropdown-tags select')
						&& !$("#checkboxes").hasClass('open')) {
					$("#checkboxes").addClass('open');
				}
			});
});