package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.vo.NewsVO;

@Controller
@RequestMapping("/admin")
public class AddNewsController {
	@Autowired
	NewsManagementService newsManagementService;

	@RequestMapping(value = "/addNews", method = RequestMethod.GET)
	public String getNewsAdmin(Model model) {
		model.addAttribute("complexNews", new NewsVO());
		return "add-news-admin";
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public String addNewsAdmin(
			@Valid @ModelAttribute("complexNews") NewsVO news,
			BindingResult result) throws ServiceException {
		news.setLocale(LocaleContextHolder.getLocale().toString());
		if (result.hasErrors()) {
			return "add-news-admin";
		}
		newsManagementService.addNewsWithAuthorAndTags(news);
		return "redirect:/admin/newsList";
	}

	@ModelAttribute("tagList")
	public List<Tag> getAllTags() throws ServiceException {
		return newsManagementService.getAllTags();
	}

	@ModelAttribute("authorList")
	public List<Author> getAllAuthors() throws ServiceException {
		return newsManagementService.getAuthorsNotExpired();
	}
}
