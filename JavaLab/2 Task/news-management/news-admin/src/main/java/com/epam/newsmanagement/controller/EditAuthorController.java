package com.epam.newsmanagement.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Controller
@RequestMapping("/admin")
public class EditAuthorController {
	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping(value = "/editAuthor")
	public String editAuthorAdmin(ModelMap model) {
		model.addAttribute("newAuthor", new Author());
		return "edit-author-admin";
	}

	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public String addAuthor(@Valid @ModelAttribute("newAuthor") Author author,
			BindingResult result) throws ServiceException {
		if (result.hasErrors()) {
			return "edit-author-admin";
		}
		newsManagementService.addAuthor(author);

		return "redirect:/admin/editAuthor";
	}

	@RequestMapping(value = "/expireAuthor/{authorId}", method = RequestMethod.GET)
	public String expireAuthor(@PathVariable Long authorId)
			throws ServiceException {
		newsManagementService.setExpired(new Author(authorId, new Date()));
		return "redirect:/admin/editAuthor";
	}

	@RequestMapping(value = "/updateAuthor", method = RequestMethod.POST)
	public String updateAuthor(@ModelAttribute("newAuthor") Author author)
			throws ServiceException {
		newsManagementService.updateAuthor(author);
		return "redirect:/admin/editAuthor";
	}

	@ModelAttribute("authorList")
	public List<Author> getAuthorsNotExpired() throws ServiceException {
		return newsManagementService.getAuthorsNotExpired();
	}
}
