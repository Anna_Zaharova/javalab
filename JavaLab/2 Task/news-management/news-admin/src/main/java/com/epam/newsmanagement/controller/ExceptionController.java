package com.epam.newsmanagement.controller;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.newsmanagement.service.exception.ServiceException;

@ControllerAdvice
public class ExceptionController {

	private static final Logger logger = Logger
			.getLogger(ExceptionController.class);

	@ExceptionHandler(ServiceException.class)
	public String handleServiceException(ServiceException ex) {
		logger.error(ex);
		return "service-error-admin";
	}

	@ExceptionHandler(Throwable.class)
	public String handleGeneralException(Exception ex, Model model) {
		logger.error(ex);
		model.addAttribute("reason", ex.getMessage());
		return "general-error-admin";
	}

}
