package com.epam.newsmanagement.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.util.ConfigManager;
import com.epam.newsmanagement.vo.Filter;

@Controller
@SessionAttributes({ "filter" })
@RequestMapping("/admin/viewNews")
public class ViewNewsController {
	@Autowired
	NewsManagementService newsManagementService;
	private final static int newsPerPage = Integer.valueOf(ConfigManager
			.getValue("newsPerPage"));

	@RequestMapping(value = "/{position}", method = RequestMethod.GET)
	public String viewNews(@PathVariable("position") int position,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException {
		int newsCount = newsManagementService.getNewsListByFilterCount(filter);
		model.addAttribute("currentNews", newsManagementService
				.getNewsListByFilter(filter, position, position).get(0));
		model.addAttribute("postComment", new Comment());
		model.addAttribute("newsCount", newsCount);
		model.addAttribute("position", position);
		model.addAttribute("newsPerPage", newsPerPage);
		return "view-news-admin";
	}

	@RequestMapping(value = "/addComment/{newsId}", method = RequestMethod.POST)
	public String addComment(@PathVariable("newsId") Long newsId,
			@RequestParam("position") int position,
			@Valid @ModelAttribute("postComment") Comment comment,
			BindingResult result) throws ServiceException {
		int pos = position;
		if (!result.hasErrors()) {
			comment.setCreationDate(new Date());
			comment.setNewsId(newsId);
			newsManagementService.addComment(comment);
			pos = newsManagementService.getPosition(newsId);
		}
		return String.format("redirect:/admin/viewNews/%d", pos);
	}

	@RequestMapping(value = "/deleteComment/{commentId}")
	public String deleteComment(@PathVariable("commentId") Long commentId,
			@RequestParam("newsId") Long newsId) throws ServiceException {
		newsManagementService.deleteCommentById(commentId);
		return String.format("redirect:/admin/viewNews/%d",
				newsManagementService.getPosition(newsId));
	}
}
