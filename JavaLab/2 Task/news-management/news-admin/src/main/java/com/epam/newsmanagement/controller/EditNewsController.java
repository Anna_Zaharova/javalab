package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.ComplexNews;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utility.DateConversionUtility;
import com.epam.newsmanagement.vo.NewsVO;

@Controller
@RequestMapping("/admin/editNews")
public class EditNewsController {
	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping("/{newsId}")
	public String viewNews(@PathVariable("newsId") Long newsId, Model model)
			throws ServiceException {
		model.addAttribute("complexNews",
				convertComplexNewsToNewsVO(newsManagementService
						.getComplexNews(newsId)));
		return "edit-news-admin";
	}

	@RequestMapping("/update")
	public String updateNews(@ModelAttribute("complexNews") NewsVO newsVO)
			throws ServiceException {
		newsVO.setLocale(LocaleContextHolder.getLocale().toString());
		newsVO.setModificationDate(new Date());
		newsManagementService.updateComplexNews(newsVO);
		return String.format("redirect:/admin/viewNews/%d",
				newsManagementService.getPosition(newsVO.getNewsId()));
	}

	@ModelAttribute("tagList")
	public List<Tag> getAllTags() throws ServiceException {
		return newsManagementService.getAllTags();
	}

	@ModelAttribute("authorList")
	public List<Author> getAllAuthors() throws ServiceException {
		return newsManagementService.getAllAuthors();
	}

	private NewsVO convertComplexNewsToNewsVO(ComplexNews complexNews) {
		NewsVO newsVO = new NewsVO();
		newsVO.setAuthorId(complexNews.getAuthor().getId());
		newsVO.setCreationDate(DateConversionUtility.convertFromDateToString(
				LocaleContextHolder.getLocale().toString(), complexNews
						.getNews().getCreationDate()));
		newsVO.setFullText(complexNews.getNews().getFullText());
		newsVO.setShortText(complexNews.getNews().getShortText());
		newsVO.setTitle(complexNews.getNews().getTitle());
		newsVO.setNewsId(complexNews.getNews().getId());
		List<Long> tags = new ArrayList<>();
		for (Tag tag : complexNews.getTags()) {
			tags.add(tag.getId());
		}
		newsVO.setTags(tags);
		return newsVO;
	}
}
