<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<ul>
	<li><a href="<c:url value="/admin/newsList" />"><spring:message
				code="news.list" /></a></li>
	<li><a href="<c:url value="/admin/addNews" />"><spring:message
				code="news.add.news" /></a></li>
	<li><a href="<c:url value="/admin/editAuthor" />"><spring:message
				code="news.update.author" /></a></li>
	<li><a href="<c:url value="/admin/editTags" />"><spring:message
				code="news.update.tag" /></a></li>
</ul>