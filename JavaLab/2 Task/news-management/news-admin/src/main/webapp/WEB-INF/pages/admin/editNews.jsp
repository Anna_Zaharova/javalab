<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="/admin/editNews/update" var="updateNews" />
<div class="add-news-container">
	<form:form action="${updateNews}" modelAttribute="complexNews"
		method="POST">
		<div class="add-news-item">
			<span class=""><spring:message code="news.title" />:</span>
			<form:input path="title" />
		</div>
		<div class="add-news-item">
			<span class=""><spring:message code="news.date" />:</span>
			<form:input path="creationDate" />
		</div>
		<div class="add-news-item">
			<span class=""><spring:message code="news.brief" />:</span>
			<form:textarea path="shortText" cssClass="add-news-brief" />
		</div>
		<div class="add-news-item">
			<span class=""><spring:message code="news.content" />:</span>
			<form:textarea path="fullText" />
		</div>
		<form:hidden path="newsId" />
		<div class="">
			<div id="dropdown" class="news-list">
				<div id="dropdown-tags">
					<select><option><spring:message
								code="label.selectTag" /></option></select>
				</div>
				<div id="checkboxes">
					<div class="tag-list">
						<span><spring:message code="label.selectTag" /></span>
						<form:checkboxes items="${tagList}" path="tags" itemLabel="name"
							itemValue="id" />
					</div>
				</div>
			</div>
			<spring:message code="label.selectAuthor" var="selectAuthor" />
			<form:select path="authorId" cssClass="news-list">
				<form:option value="" label="${selectAuthor }" />
				<c:forEach items="${authorList}" var="author">
					<form:option value="${author.id}">${author.name}</form:option>
				</c:forEach>
			</form:select>
		</div>
		<input type="submit" class="news-save-btn"
			value="<spring:message code="button.save" />" />
	</form:form>
</div>