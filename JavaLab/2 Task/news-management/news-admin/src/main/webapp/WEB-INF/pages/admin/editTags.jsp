<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="edit-tags-container">
	<form:form action="" modelAttribute="newTag" id="tag-form"
		cssStyle='display:none' method="POST">
		<form:input path="name" />
		<form:input path="id" />
	</form:form>
	<c:forEach items="${tagList}" var="tag">
		<div class="edit-tag">
			<span class="tag-title"><spring:message code="news.tag" />:</span> <input
				type="text" class="tag-name" value="${tag.name}" disabled /> <input
				type="hidden" value="${tag.id}" /> <a href=""
				onclick="return editTag(this)" class="editTag"> <spring:message
					code="news.edit" /></a> <a href="" onclick="return updateTag(this)"
				style='display: none;'><spring:message code="news.update" /></a> <a
				href="<c:url value="/admin/deleteTag/${tag.id}"/>"
				style='display: none;'><spring:message code="news.delete" /> </a> <a
				href="" style='display: none;' onclick="return cancel(this)"><spring:message
					code="news.cancel" /></a>
		</div>
	</c:forEach>
	<div class="add-tag">
		<span class="tag-title"><spring:message code="news.add.tag" />:</span>
		<input type="text" class="tag-name" value="" /> <a href=""
			onclick="return addTag(this)"><spring:message code="button.save" /></a>
	</div>
</div>