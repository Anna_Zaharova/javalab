<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="header">
	<c:url value="/j_spring_security_logout" var="logout" />
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<form action="${logout}" method="post">
			<button type="submit" class="logout">
				<spring:message code="button.logout" />
			</button>
		</form>
		<span class="user"><spring:message code="label.hello" />, <security:authentication
				property="principal.username" /></span>
	</security:authorize>
	<div class="clear"></div>
	<div class="language">
		<ul>
			<li><a href="?lang=en">EN</a></li>
			<li><a href="?lang=ru">RU</a></li>
		</ul>
	</div>
	<h1>
		<security:authorize access="hasRole('ROLE_ADMIN')">
			<spring:message code="header.admin" />
		</security:authorize>
		<security:authorize access="hasRole('ROLE_ANONYMOUS')">
			<spring:message code="header.user" />
		</security:authorize>
	</h1>
</div>