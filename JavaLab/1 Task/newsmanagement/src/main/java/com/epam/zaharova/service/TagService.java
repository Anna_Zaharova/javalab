package com.epam.zaharova.service;

import java.util.List;

import com.epam.zaharova.entity.Tag;
import com.epam.zaharova.service.exception.ServiceException;

public interface TagService {
	/**
	 * Receives the tag by identifier
	 * 
	 * @param tagId
	 *            the tag identifier
	 * @return the tag
	 * @throws ServiceException
	 */
	Tag getTagById(Long tagId) throws ServiceException;

	/**
	 * Edits the information about the tag
	 * 
	 * @param tag
	 *            tag to edit
	 * @throws ServiceException
	 */
	void updateTag(Tag tag) throws ServiceException;

	/**
	 * Deletes the tag by identifier
	 * 
	 * @param tagId
	 *            the tag identifier
	 * @throws ServiceException
	 */
	void deleteTagById(Long tagId) throws ServiceException;

	/**
	 * Adds the tag to the data source
	 * 
	 * @param tag
	 *            tag to add
	 * @throws ServiceException
	 */
	void addTag(Tag tag) throws ServiceException;

	/**
	 * Receives the list of tags by news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the list of tags
	 * @throws ServiceException
	 */
	List<Tag> getNewsTag(Long newsId) throws ServiceException;

	/**
	 * Returns all existing tags
	 * 
	 * @return tags list
	 * @throws ServiceException
	 */
	List<Tag> getAllTags() throws ServiceException;
}
