package com.epam.zaharova.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zaharova.dao.NewsDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.News;
import com.epam.zaharova.utility.DAOutility;

public class NewsDAOImp implements NewsDAO {
	private final static String SQL_READ_NEWS_BY_AUTHOR = "SELECT N.news_id, N.short_text, N.full_text, N.title, N.creation_date, "
			+ "N.modification_date FROM News N INNER JOIN News_Author NA ON N.news_id = NA.news_id WHERE NA.author_id = ?";
	private final static String SQL_READ_ALL_NEWS = "SELECT news_id, short_text, full_text, title, creation_date, modification_date FROM News";
	private final static String SQL_ADD_NEWS_TAG = "INSERT INTO News_Tag(news_id, tag_id) VALUE (?, ?)";
	private final static String SQL_ADD_NEWS_AUTHOR = "INSERT INTO News_Author(news_id, author_id) VALUES(?, ?)";
	private final static String SQL_READ_MOST_COMMENTED_NEWS = "SELECT  N.news_id, N.short_text, N.full_text, N.title, N.creation_date, "
			+ "N.modification_date FROM News N INNER JOIN (SELECT news_id , COUNT(*) as comments_amount FROM Comments"
			+ " GROUP BY news_id ORDER BY comments_amount DESC) C ON N.news_id = C.news_id";
	private final static String SQL_INSERT_NEWS = "INSERT INTO News(news_id, short_text, full_text, title, creation_date, "
			+ "modification_date) VALUES (news_seq.nextval, ?, ?, ?, ?, ?)";
	private final static String SQL_DELETE_NEWS = "DELETE FROM News N, News_Author NA, News_Tag NT,"
			+ "Comments C WHERE N.news_id = NA.news_id AND N.news_id = NT.news_id AND N.news_id = C.news_id"
			+ "AND N.news_id =?";
	private final static String SQL_READ_NEWS = "SELECT news_id, short_text, full_text, title, creation_date, modification_date"
			+ " FROM News WHERE news_id = ?";
	private final static String SQL_UPDATE_NEWS = "UPDATE News SET short_text = ?, full_text = ?, title = ?, "
			+ "modification_date = ? WHERE news_id = ?";
	private static String SQL_READ_NEWS_BY_TAGS = "SELECT DISTINCT N.news_id, N.short_text, N.full_text, N.title, N.creation_date, "
			+ "N.modification_date FROM NEWS N INNER JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID WHERE  NT.TAG_ID IN(";
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long create(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		long id = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS,
					new String[] { "news_id" });
			prepareStatementForCreate(preparedStatement, news);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the news not created");
			}
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
				news.setId(id);
			} else {
				throw new DAOException("the key not generated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return id;
	}

	@Override
	public News read(Long id) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_NEWS);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createNews(resultSet);
			} else {
				throw new DAOException("the news with id = " + id
						+ " not found");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public void update(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			prepareStatementForUpdate(preparedStatement, news);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the news not updated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the news not deleted");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public News readNewsByAuthor(Long authorId) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			try {
				connection = DataSourceUtils.getConnection(dataSource);
				preparedStatement = connection
						.prepareStatement(SQL_READ_NEWS_BY_AUTHOR);
				preparedStatement.setLong(1, authorId);
				resultSet = preparedStatement.executeQuery();
				if (resultSet.next()) {
					return createNews(resultSet);
				} else
					throw new DAOException("the news with author_id = "
							+ authorId + "not found");
			} finally {
				DAOutility.closeResources(dataSource, connection,
						preparedStatement, resultSet);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<News> readNewsByTags(List<Long> tagsId) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			generateQuery(tagsId.size());
			preparedStatement = connection
					.prepareStatement(SQL_READ_NEWS_BY_TAGS);
			for (int i = 0; i < tagsId.size(); i++) {
				preparedStatement.setLong(i + 1, tagsId.get(i));
			}
			resultSet = preparedStatement.executeQuery();
			List<News> news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createNews(resultSet));
			}
			return news;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public List<News> readAll() throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_ALL_NEWS);
			List<News> news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createNews(resultSet));
			}
			return news;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection, statement,
					resultSet);
		}
	}

	@Override
	public void addNewsTag(Long newsId, Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			try {
				connection = DataSourceUtils.getConnection(dataSource);
				preparedStatement = connection
						.prepareStatement(SQL_ADD_NEWS_TAG);
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				if (preparedStatement.executeUpdate() != 1) {
					throw new DAOException("the tag not added to the news");
				}
			} finally {
				DAOutility.closeResources(dataSource, connection,
						preparedStatement);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public void addNewsAuthor(Long newsId, Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_ADD_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the author not added to the news");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<News> readMostCommentedNews() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_READ_MOST_COMMENTED_NEWS);
			resultSet = preparedStatement.executeQuery();
			ArrayList<News> news = new ArrayList<>();
			while (resultSet.next()) {
				news.add(createNews(resultSet));
			}
			return news;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	private void prepareStatementForCreate(PreparedStatement preparedStatement,
			News news) throws SQLException {
		preparedStatement.setString(1, news.getShortText());
		preparedStatement.setString(2, news.getFullText());
		preparedStatement.setString(3, news.getTitle());
		preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate()
				.getTime()));
		preparedStatement.setDate(5, new java.sql.Date(news
				.getModificationDate().getTime()));
	}

	private News createNews(ResultSet resultSet) throws SQLException {
		News news = new News();
		news.setCreationDate(resultSet.getTimestamp("creation_date"));
		news.setFullText(resultSet.getString("full_text"));
		news.setId(resultSet.getLong("news_id"));
		news.setModificationDate(new Date(resultSet
				.getDate("modification_date").getTime()));
		news.setShortText(resultSet.getString("short_text"));
		news.setTitle(resultSet.getString("title"));
		return news;
	}

	private void prepareStatementForUpdate(PreparedStatement preparedStatement,
			News news) throws SQLException {
		preparedStatement.setString(1, news.getShortText());
		preparedStatement.setString(2, news.getFullText());
		preparedStatement.setString(3, news.getTitle());
		preparedStatement.setDate(4, new java.sql.Date(news
				.getModificationDate().getTime()));
		preparedStatement.setLong(5, news.getId());
	}

	private void generateQuery(int length) {
		StringBuilder sb = new StringBuilder();
		sb.append(SQL_READ_NEWS_BY_TAGS);
		for (int i = 0; i < length; i++) {
			sb.append("?");
			if (i != length - 1) {
				sb.append(",");
			}
		}
		sb.append(")");
		SQL_READ_NEWS_BY_TAGS = sb.toString();
	}
}
