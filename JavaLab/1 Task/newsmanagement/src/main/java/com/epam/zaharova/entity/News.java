package com.epam.zaharova.entity;

import java.util.Date;

public class News extends Entity {

	private static final long serialVersionUID = 620479990306142700L;

	private String shortText;
	private String fullText;
	private String title;
	private Date creationDate;
	private Date modificationDate;

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public String toString() {
		return super.toString() + "\nshortText = " + shortText
				+ "\nfullText = " + fullText + "\ntitle = " + title
				+ "\ncreationDate = " + creationDate + "\nmodificationDate = "
				+ modificationDate;

	}

	@Override
	public int hashCode() {
		int hash = 17, result = 0;
		int superHash = super.hashCode();
		result = hash * superHash
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = hash * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = hash
				* result
				+ superHash
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = hash * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = hash * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
