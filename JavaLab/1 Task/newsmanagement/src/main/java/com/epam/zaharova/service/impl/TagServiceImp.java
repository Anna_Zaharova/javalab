package com.epam.zaharova.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.zaharova.dao.TagDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Tag;
import com.epam.zaharova.service.TagService;
import com.epam.zaharova.service.exception.ServiceException;

@Transactional(rollbackFor = ServiceException.class)
public class TagServiceImp implements TagService {
	private TagDAO tagDAO;

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	@Override
	public Tag getTagById(Long tagId) throws ServiceException {
		try {
			return tagDAO.read(tagId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void updateTag(Tag tag) throws ServiceException {
		try {
			tagDAO.update(tag);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void deleteTagById(Long tagId) throws ServiceException {
		try {
			tagDAO.delete(tagId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void addTag(Tag tag) throws ServiceException {
		try {
			tagDAO.create(tag);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Tag> getNewsTag(Long newsId) throws ServiceException {
		try {
			return tagDAO.readNewsTag(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Tag> getAllTags() throws ServiceException {
		try {
			return tagDAO.readAll();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}
}
