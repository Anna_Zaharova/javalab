package com.epam.zaharova.service;

import java.util.List;

import com.epam.zaharova.entity.ComplexNews;
import com.epam.zaharova.service.exception.ServiceException;

public interface NewsManagementService {

	/**
	 * Returns the complex news by identifier
	 * 
	 * @param newsId
	 *            the complex news identifier
	 * @return the complex news
	 * @throws ServiceException
	 */
	public ComplexNews getComplexNews(Long newsId) throws ServiceException;

	/**
	 * Adds news with information about author and tags
	 * 
	 * @param complexNews
	 *            the complex news
	 * @throws ServiceException
	 */
	void addNewsWithAuthorAndTags(ComplexNews complexNews)
			throws ServiceException;

	/**
	 * Returns the most commented complex news
	 * 
	 * @return the list of complex news
	 * @throws ServiceException
	 */
	List<ComplexNews> getMostCommentedComplexNews() throws ServiceException;

	/**
	 * Deletes all information about news
	 * 
	 * @param complexNews
	 *            complex news
	 * @throws ServiceException
	 */
	void deleteComplexNews(ComplexNews complexNews) throws ServiceException;

}
