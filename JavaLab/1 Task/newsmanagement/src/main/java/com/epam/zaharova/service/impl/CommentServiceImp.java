package com.epam.zaharova.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.zaharova.dao.CommentDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Comment;
import com.epam.zaharova.service.CommentService;
import com.epam.zaharova.service.exception.ServiceException;

@Transactional(rollbackFor = ServiceException.class)
public class CommentServiceImp implements CommentService {
	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	@Override
	public void updateComment(Comment comment) throws ServiceException {
		try {
			commentDAO.update(comment);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void deleteCommentById(Long commentId) throws ServiceException {
		try {
			commentDAO.delete(commentId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public Comment getCommentById(Long commentId) throws ServiceException {
		try {
			return commentDAO.read(commentId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void addComment(Comment comment) throws ServiceException {
		try {
			commentDAO.create(comment);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Comment> getNewsComments(Long newsId) throws ServiceException {
		try {
			return commentDAO.readNewsComments(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}
}
