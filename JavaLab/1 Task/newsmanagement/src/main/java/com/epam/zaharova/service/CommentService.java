package com.epam.zaharova.service;

import java.util.List;

import com.epam.zaharova.entity.Comment;
import com.epam.zaharova.service.exception.ServiceException;

public interface CommentService {
	/**
	 * Receives comment by identifier
	 * 
	 * @param commentId
	 *            the comment identifier
	 * @return the comment
	 * @throws ServiceException
	 */
	Comment getCommentById(Long commentId) throws ServiceException;

	/**
	 * Adds the comment information
	 * 
	 * @param comment
	 *            the comment to add
	 * @throws ServiceException
	 */
	void addComment(Comment comment) throws ServiceException;

	/**
	 * Edits the comment information
	 * 
	 * @param comment
	 *            the comment to edit
	 * @throws ServiceException
	 */
	void updateComment(Comment comment) throws ServiceException;

	/**
	 * Deletes the comment by identifier
	 * 
	 * @param commentId
	 *            comment identifier
	 * @throws ServiceException
	 */
	void deleteCommentById(Long commentId) throws ServiceException;

	/**
	 * Receives the list of comments by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @return the list of comments
	 * @throws ServiceException
	 */
	List<Comment> getNewsComments(Long newsId) throws ServiceException;

}
