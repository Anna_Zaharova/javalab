package com.epam.zaharova.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zaharova.dao.TagDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Tag;
import com.epam.zaharova.utility.DAOutility;

public class TagDAOImp implements TagDAO {
	private final static String SQL_READ_NEWS_TAG = "SELECT T.tag_id, T.tag_name FROM Tag T INNER JOIN News_Tag NT"
			+ "ON T.tag_id = NT.tag_id WHERE NT.news_id = ?";
	private final static String SQL_READ_TAG_BY_NAME = "SELECT tag_id, tag_name FROM Tag WHERE tag_name = ?";
	private final static String SQL_INSERT_TAG = "INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, ?)";
	private final static String SQL_UPDATE_TAG = "UPDATE Tag SET tag_name = ? WHERE tag_id = ?";
	private final static String SQL_DELETE_TAG = "DELETE FROM Tag WHERE tag_id = ?";
	private final static String SQL_READ_TAG = "SELECT tag_id, tag_name FROM Tag WHERE tag_id = ?";
	private final static String SQL_READ_ALL_TAGS = "SELECT tag_id, tag_name  FROM Tag";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<Tag> readNewsTag(Long newsId) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_NEWS_TAG);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			List<Tag> tags = Collections.emptyList();
			while (resultSet.next()) {
				tags.add(createTag(resultSet));
			}
			return tags;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public Tag readTagByName(String tagName) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			try {
				connection = DataSourceUtils.getConnection(dataSource);
				preparedStatement = connection
						.prepareStatement(SQL_READ_TAG_BY_NAME);
				preparedStatement.setString(1, tagName);
				resultSet = preparedStatement.executeQuery();
				if (resultSet.next()) {
					return createTag(resultSet);
				}
				throw new DAOException("the tag not found");
			} finally {
				DAOutility.closeResources(dataSource, connection,
						preparedStatement, resultSet);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public Long create(Tag tag) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		long id = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_TAG,
					new String[] { "tag_id" });
			preparedStatement.setString(1, tag.getName());
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the tag not created");
			}
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
				tag.setId(id);
			} else {
				throw new DAOException("the key not generated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return id;
	}

	@Override
	public Tag read(Long id) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_TAG);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createTag(resultSet);
			} else {
				throw new DAOException("the tag  with id = " + id + "not found");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public void update(Tag tag) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
			preparedStatement.setString(1, tag.getName());
			preparedStatement.setLong(2, tag.getId());
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the tag not updated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the tag not deleted");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	private Tag createTag(ResultSet resultSet) throws SQLException {
		Tag tag = new Tag();
		tag.setId(resultSet.getLong("tag_id"));
		tag.setName(resultSet.getString("tag_name"));
		return tag;
	}

	@Override
	public List<Tag> readAll() throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_ALL_TAGS);
			List<Tag> tags = new ArrayList<>();
			while (resultSet.next()) {
				tags.add(createTag(resultSet));
			}
			return tags;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection, statement,
					resultSet);
		}
	}
}
