package com.epam.zaharova.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.zaharova.dao.AuthorDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Author;
import com.epam.zaharova.service.AuthorService;
import com.epam.zaharova.service.exception.ServiceException;

@Transactional(rollbackFor = ServiceException.class)
public class AuthorServiceImp implements AuthorService {
	private AuthorDAO authorDAO;

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	@Override
	public void addAuthor(Author author) throws ServiceException {
		try {
			authorDAO.create(author);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void deleteAuthorById(Long authorId) throws ServiceException {
		try {
			authorDAO.delete(authorId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void updateAuthor(Author author) throws ServiceException {
		try {
			authorDAO.update(author);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public Author getAuthorById(Long authorId) throws ServiceException {
		try {
			return authorDAO.read(authorId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public Author getNewsAuthor(Long newsId) throws ServiceException {
		try {
			return authorDAO.readNewsAuthor(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void setExpired(Author author) throws ServiceException {
		try {
			authorDAO.setExpired(author);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<Author> getAllAuthors() throws ServiceException {
		try {
			return authorDAO.readAll();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

}
