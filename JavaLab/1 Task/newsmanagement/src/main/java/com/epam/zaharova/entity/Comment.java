package com.epam.zaharova.entity;

import java.util.Date;

public class Comment extends Entity {

	private static final long serialVersionUID = 4484659598640469996L;
	private String commentText;
	private Date creationDate;
	private Long newsId;

	public Comment() {

	}

	public Comment(String commentText, Date creationDate) {
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	@Override
	public String toString() {
		return super.toString() + "\ncommentText = " + commentText
				+ "\ncreationDate" + creationDate;
	}

	@Override
	public int hashCode() {
		int hash = 13, result = 0;
		int superHash = super.hashCode();
		result = hash * superHash
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = hash * result + superHash
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = hash * result + (int) (newsId ^ (newsId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId != other.newsId)
			return false;
		return true;
	}

}
