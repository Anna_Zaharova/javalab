package com.epam.zaharova.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zaharova.dao.CommentDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Comment;
import com.epam.zaharova.utility.DAOutility;

public class CommentDAOImp implements CommentDAO {
	private static final String SQL_READ_NEWS_COMMENTS = "SELECT comment_id, comment_text, creation_date, news_id FROM Comments WHERE news_id = ?";
	private final static String SQL_INSERT_COMMENT = "INSERT INTO Comments(comment_id, comment_text, creation_date, news_id)"
			+ " VALUES(comments_seq.nextval, ?, ?, ?)";
	private final static String SQL_READ_COMMENT = "SELECT comment_id, comment_text, creation_date, news_id"
			+ " FROM Comments WHERE comment_id = ?";
	private final static String SQL_UPDATE_COMMENT = "UPDATE Comments SET comment_text = ? WHERE comment_id = ?";
	private final static String SQL_DELETE_COMMENT = "DELETE FROM Comments WHERE comment_id = ?";
	private final static String SQL_READ_ALL_COMMENTS = "SELECT comment_id, comment_text, creation_date, news_id FROM Comments";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long create(Comment comment) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		long id = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_COMMENT,
					new String[] { "comment_id" });
			prepareStatementForCreate(preparedStatement, comment);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the comment not created");
			}
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
				comment.setId(id);
			} else {
				throw new DAOException("the key not generated");
			}

		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return id;
	}

	private void prepareStatementForCreate(PreparedStatement preparedStatement,
			Comment comment) throws SQLException {
		preparedStatement.setString(1, comment.getCommentText());
		preparedStatement.setTimestamp(2, new Timestamp(comment
				.getCreationDate().getTime()));
		preparedStatement.setLong(3, comment.getNewsId());

	}

	@Override
	public List<Comment> readNewsComments(Long newsId) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_READ_NEWS_COMMENTS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			List<Comment> comments = new ArrayList<>();
			while (resultSet.next()) {
				comments.add(createComment(resultSet));
			}
			return comments;

		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public Comment read(Long id) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_COMMENT);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createComment(resultSet);
			} else {
				throw new DAOException("the comment not found");
			}

		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	private Comment createComment(ResultSet resultSet) throws SQLException {
		Comment comment = new Comment();
		comment.setId(resultSet.getLong("comment_id"));
		comment.setCommentText(resultSet.getString("comment_text"));
		comment.setNewsId(resultSet.getLong("news_id"));
		comment.setCreationDate(resultSet.getTimestamp("creation_date"));

		return comment;
	}

	@Override
	public void update(Comment comment) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			preparedStatement.setString(1, comment.getCommentText());
			preparedStatement.setLong(2, comment.getId());
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the comment not updated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the comment not deleted");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<Comment> readAll() throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_ALL_COMMENTS);
			List<Comment> comments = new ArrayList<>();
			while (resultSet.next()) {
				comments.add(createComment(resultSet));
			}
			return comments;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection, statement,
					resultSet);
		}
	}

}
