package com.epam.zaharova.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zaharova.dao.UserDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.User;
import com.epam.zaharova.utility.DAOutility;

public class UserDAOImp implements UserDAO {

	private static final String SQL_READ_USER_BY_LOGIN_AND_PASSWORD = ""
			+ "SELECT * FROM Users WHERE login = ? AND password = ?";
	private static final String SQL_INSERT_USER = "INSERT INTO Users(user_id, user_name, login, password)"
			+ "(user_seq.nextval, ?, ?, ?)";
	private static final String SQL_DELETE_USER = "DELETE FROM Users U, Roles R WHERE U.user_id = R.user_id"
			+ "AND U.user_id = ?";
	private static final String SQL_UPDATE_USER = "UPDATE Users SET user_name = ?, login = ?, password = ?"
			+ "WHERE user_id = ?";
	private static final String SQL_READ_USER = "SELECT user_id, user_name, login, password"
			+ " FROM Users WHERE user_id = ?";
	private static final String SQL_DELETE_USER_ROLE = "DELETE FROM Roles WHERE user_id = ? AND role_name = ?";
	private static final String SQL_ADD_USER_ROLE = "INSERT INTO Roles(user_id, role_name) VALUES(?, ?)";
	private static final String SQL_READ_USER_ROLES = "SELECT role_name FROM Roles WHERE user_id = ?";
	private static final String SQL_READ_ALL_USERS = "SELECT user_id, user_name, login, password FROM Users";
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long create(User user) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		long id = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_USER,
					new String[] { "user_id" });
			prepareStatementForCreate(preparedStatement, user);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the user not created");
			}
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
				user.setId(id);
			} else {
				throw new DAOException("the key not generated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
		return id;
	}

	private void prepareStatementForCreate(PreparedStatement preparedStatement,
			User user) throws SQLException {
		preparedStatement.setString(1, user.getUserName());
		preparedStatement.setString(2, user.getLogin());
		preparedStatement.setString(3, user.getPassword());
	}

	@Override
	public User read(Long id) throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_READ_USER);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createUser(resultSet);
			} else {
				throw new DAOException("the user not found");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	@Override
	public void update(User user) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);
			prepareStatementForUpdate(preparedStatement, user);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the user not updated");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	private void prepareStatementForUpdate(PreparedStatement preparedStatement,
			User user) throws SQLException {
		preparedStatement.setString(1, user.getUserName());
		preparedStatement.setString(2, user.getLogin());
		preparedStatement.setString(3, user.getPassword());
		preparedStatement.setLong(4, user.getId());
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_USER);
			preparedStatement.setLong(1, id);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the user not deleted");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public User readUserByLoginAndPassword(String login, String password)
			throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_READ_USER_BY_LOGIN_AND_PASSWORD);
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return createUser(resultSet);
			}
			throw new DAOException("the user not found!");
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection,
					preparedStatement, resultSet);
		}
	}

	private User createUser(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setId(resultSet.getLong("user_id"));
		user.setLogin(resultSet.getString("login"));
		user.setPassword(resultSet.getString("password"));
		user.setUserName(resultSet.getString("user_name"));
		return user;
	}

	@Override
	public void addUserRole(Long userId, String roleName) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD_USER_ROLE);
			preparedStatement.setLong(1, userId);
			preparedStatement.setString(2, roleName);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the user's role not added!");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void deleteUserRole(Long userId, String roleName)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_USER_ROLE);
			preparedStatement.setLong(1, userId);
			preparedStatement.setString(2, roleName);
			if (preparedStatement.executeUpdate() != 1) {
				throw new DAOException("the user's role not deleted!");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public void deleteUserRoles(Long userId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_USER_ROLE);
			preparedStatement.setLong(1, userId);
			if (preparedStatement.executeUpdate() <= 0) {
				throw new DAOException("the users roles not deleted!");
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<String> readUserRoles(Long userId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_READ_USER_ROLES);
			preparedStatement.setLong(1, userId);
			resultSet = preparedStatement.executeQuery();
			List<String> roles = new ArrayList<>(3);
			while (resultSet.next()) {
				roles.add(resultSet.getString("role_name"));
			}
			return roles;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility
					.closeResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<User> readAll() throws DAOException {
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_ALL_USERS);
			List<User> users = new ArrayList<>();
			while (resultSet.next()) {
				users.add(createUser(resultSet));
			}
			return users;
		} catch (SQLException ex) {
			throw new DAOException(ex);
		} finally {
			DAOutility.closeResources(dataSource, connection, statement,
					resultSet);
		}
	}
}
