package com.epam.zaharova.service;

import java.util.List;

import com.epam.zaharova.entity.Author;
import com.epam.zaharova.entity.News;
import com.epam.zaharova.service.exception.ServiceException;

public interface NewsService {
	/**
	 * Edits the information about the news
	 * 
	 * @param news
	 *            news to edit
	 * @throws ServiceException
	 */
	void updateNews(News news) throws ServiceException;

	/**
	 * Returns the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the news
	 * @throws ServiceException
	 */
	News getNewsById(Long newsId) throws ServiceException;

	/**
	 * Deletes the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @throws ServiceException
	 */
	void deleteNewsById(Long newsId) throws ServiceException;

	/**
	 * Adds the news to the data source
	 * 
	 * @param news
	 *            news to add
	 * @throws ServiceException
	 */
	void addNews(News news) throws ServiceException;

	/**
	 * Adds new tag to the news
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param tagId
	 *            the tag identifier
	 * @throws ServiceException
	 */
	void addNewsTag(Long newsId, Long tagId) throws ServiceException;

	/**
	 * Adds new author to the news
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param authorId
	 *            the author identifier
	 * @throws ServiceException
	 */
	void addNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Returns all news from data source
	 * 
	 * @return the list of news
	 * @throws ServiceException
	 */
	List<News> getAllNews() throws ServiceException;

	/**
	 * Returns most commented news
	 * 
	 * @return the list of news
	 * @throws ServiceException
	 */
	List<News> getMostCommentedNews() throws ServiceException;

	/**
	 * Returns news by list of tags
	 * 
	 * @param tags
	 *            the list of tags
	 * @return the list of news
	 * @throws ServiceException
	 */
	List<News> searchNewsByTags(List<Long> tags) throws ServiceException;

	/**
	 * Returns news by author
	 * 
	 * @param author
	 *            author
	 * @return the news
	 * @throws ServiceException
	 */
	News searchNewsByAuthor(Author author) throws ServiceException;

}
