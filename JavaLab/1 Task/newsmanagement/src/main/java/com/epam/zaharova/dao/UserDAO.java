package com.epam.zaharova.dao;

import java.util.List;

import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.User;

public interface UserDAO extends AbstractDAO<User, Long> {
	/**
	 * Returns the user with the certain login and password
	 * 
	 * @param login
	 *            the user login
	 * @param password
	 *            the user password
	 * @return the user
	 * @throws DAOException
	 */
	User readUserByLoginAndPassword(String login, String password)
			throws DAOException;

	/**
	 * Adds new role to the user
	 * 
	 * @param userId
	 *            the user identifier
	 * @param roleName
	 *            the role name
	 * @throws DAOException
	 */
	void addUserRole(Long userId, String roleName) throws DAOException;

	/**
	 * Deletes the certain user role
	 * 
	 * @param userId
	 *            the user identifier
	 * @param roleName
	 *            the role name
	 * @throws DAOException
	 */
	void deleteUserRole(Long userId, String roleName) throws DAOException;

	/**
	 * Deletes all user roles
	 * 
	 * @param userId
	 *            the user identifier
	 * @throws DAOException
	 */
	public void deleteUserRoles(Long userId) throws DAOException;

	/**
	 * Returns all user roles
	 * 
	 * @param userId
	 *            the user identifier
	 * @return the list of user roles
	 * @throws DAOException
	 */
	public List<String> readUserRoles(Long userId) throws DAOException;
}
