package com.epam.zaharova.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.zaharova.entity.Author;
import com.epam.zaharova.entity.Comment;
import com.epam.zaharova.entity.ComplexNews;
import com.epam.zaharova.entity.News;
import com.epam.zaharova.entity.Tag;
import com.epam.zaharova.service.AuthorService;
import com.epam.zaharova.service.CommentService;
import com.epam.zaharova.service.NewsManagementService;
import com.epam.zaharova.service.NewsService;
import com.epam.zaharova.service.TagService;
import com.epam.zaharova.service.exception.ServiceException;

@Transactional(rollbackFor = ServiceException.class)
public class NewsManagementServiceImp implements NewsManagementService {
	private NewsService newsService;
	private CommentService commentService;
	private TagService tagService;
	private AuthorService authorService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	@Override
	public void addNewsWithAuthorAndTags(ComplexNews complexNews)
			throws ServiceException {
		News news = complexNews.getNews();
		Author author = complexNews.getAuthor();
		newsService.addNews(news);
		authorService.addAuthor(author);
		for (Tag tag : complexNews.getTags()) {
			tagService.addTag(tag);
		}
		newsService.addNewsAuthor(news.getId(), author.getId());
		for (Tag tag : complexNews.getTags()) {
			newsService.addNewsTag(news.getId(), tag.getId());
		}

	}

	@Override
	public ComplexNews getComplexNews(Long newsId) throws ServiceException {
		ComplexNews complexNews = new ComplexNews();
		complexNews.setNews(newsService.getNewsById(newsId));
		complexNews.setAuthor(authorService.getNewsAuthor(newsId));
		complexNews.setTags(tagService.getNewsTag(newsId));
		complexNews.setComments(commentService.getNewsComments(newsId));
		return complexNews;
	}

	@Override
	public List<ComplexNews> getMostCommentedComplexNews()
			throws ServiceException {
		List<ComplexNews> complexNewsList = new ArrayList<>();
		List<News> newsList = newsService.getMostCommentedNews();
		for (News news : newsList) {
			ComplexNews complexNews = new ComplexNews();
			Long newsId = news.getId();

			complexNews.setNews(news);
			complexNews.setAuthor(authorService.getNewsAuthor(newsId));
			complexNews.setComments(commentService.getNewsComments(newsId));
			complexNews.setTags(tagService.getNewsTag(newsId));

			complexNewsList.add(complexNews);
		}
		return complexNewsList;
	}

	@Override
	public void deleteComplexNews(ComplexNews complexNews)
			throws ServiceException {
		News news = complexNews.getNews();
		newsService.deleteNewsById(news.getId());
		authorService.setExpired(complexNews.getAuthor());
		for (Tag tag : complexNews.getTags()) {
			tagService.deleteTagById(tag.getId());
		}
		for (Comment comment : complexNews.getComments()) {
			commentService.deleteCommentById(comment.getId());
		}
	}
}
