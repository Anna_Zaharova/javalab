package com.epam.zaharova.service;

import java.util.List;

import com.epam.zaharova.entity.Author;
import com.epam.zaharova.service.exception.ServiceException;

public interface AuthorService {
	/**
	 * Adds the author to the data source
	 * 
	 * @param author
	 *            author to add
	 * @throws ServiceException
	 */
	void addAuthor(Author author) throws ServiceException;

	/**
	 * Deletes the author by identifier
	 * 
	 * @param authorId
	 *            the author identifier
	 * @throws ServiceException
	 */
	void deleteAuthorById(Long authorId) throws ServiceException;

	/**
	 * Edits the information about the author
	 * 
	 * @param author
	 *            author to edit
	 * @throws ServiceException
	 */
	void updateAuthor(Author author) throws ServiceException;

	/**
	 * Returns the author by identifier
	 * 
	 * @param authorId
	 *            the author identifier
	 * @return the author
	 * @throws ServiceException
	 */
	Author getAuthorById(Long authorId) throws ServiceException;

	/**
	 * Returns the author by news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the author
	 * @throws ServiceException
	 */
	Author getNewsAuthor(Long newsId) throws ServiceException;

	/**
	 * Sets expired value to the author
	 * 
	 * @param author
	 *            the author
	 * @throws ServiceException
	 */
	void setExpired(Author author) throws ServiceException;

	/**
	 * Returns all existing authors
	 * 
	 * @return authors list
	 * @throws ServiceException
	 */
	List<Author> getAllAuthors() throws ServiceException;
}
