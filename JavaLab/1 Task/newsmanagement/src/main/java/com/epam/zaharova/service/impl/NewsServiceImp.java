package com.epam.zaharova.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.zaharova.dao.NewsDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Author;
import com.epam.zaharova.entity.News;
import com.epam.zaharova.service.NewsService;
import com.epam.zaharova.service.exception.ServiceException;

@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImp implements NewsService {
	NewsDAO newsDAO;

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	@Override
	public void deleteNewsById(Long id) throws ServiceException {
		try {
			newsDAO.delete(id);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void updateNews(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public News getNewsById(Long newsId) throws ServiceException {
		try {
			return newsDAO.read(newsId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<News> searchNewsByTags(List<Long> tags) throws ServiceException {
		try {
			return newsDAO.readNewsByTags(tags);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public News searchNewsByAuthor(Author author) throws ServiceException {
		try {
			return newsDAO.readNewsByAuthor(author.getId());
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void addNews(News news) throws ServiceException {
		try {
			newsDAO.create(news);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void addNewsTag(Long newsId, Long tagId) throws ServiceException {
		try {
			newsDAO.addNewsTag(newsId, tagId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public void addNewsAuthor(Long newsId, Long authorId)
			throws ServiceException {
		try {
			newsDAO.addNewsAuthor(newsId, authorId);
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<News> getAllNews() throws ServiceException {
		try {
			return newsDAO.readAll();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

	@Override
	public List<News> getMostCommentedNews() throws ServiceException {
		try {
			return newsDAO.readMostCommentedNews();
		} catch (DAOException ex) {
			throw new ServiceException(ex);
		}
	}

}
