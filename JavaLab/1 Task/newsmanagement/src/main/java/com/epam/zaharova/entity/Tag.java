package com.epam.zaharova.entity;

public class Tag extends Entity {

	private static final long serialVersionUID = 6978879739174681227L;
	private String name;

	public Tag() {

	}

	public Tag(Long id, String name) {
		super(id);
		this.name = name;
	}

	public Tag(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return super.toString() + "\ntag_name = " + name;
	}

	@Override
	public int hashCode() {
		int hash = 13;
		return (hash * super.hashCode() + ((name == null) ? 0 : name.hashCode()));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
