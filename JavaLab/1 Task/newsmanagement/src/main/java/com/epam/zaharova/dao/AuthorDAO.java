package com.epam.zaharova.dao;

import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Author;

/**
 * Provides extra operations for manipulate author data from the data source
 * 
 *
 */
public interface AuthorDAO extends AbstractDAO<Author, Long> {
	/**
	 * Receives the author by news identifier
	 * 
	 * @param newsID
	 *            news identifier
	 * @return the author which was found
	 * @throws DAOException
	 */
	Author readNewsAuthor(Long newsID) throws DAOException;

	/**
	 * Sets an author expired value
	 * 
	 * @param author
	 *            an author which will be update
	 * @throws DAOException
	 */
	void setExpired(Author author) throws DAOException;
}
