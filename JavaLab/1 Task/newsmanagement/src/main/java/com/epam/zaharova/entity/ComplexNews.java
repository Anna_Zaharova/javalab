package com.epam.zaharova.entity;

import java.io.Serializable;
import java.util.List;

public class ComplexNews implements Serializable {

	private static final long serialVersionUID = 7443955991076166976L;
	private News news;
	private Author author;
	private List<Comment> comments;
	private List<Tag> tags;

	public ComplexNews() {
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "ComplexNews [news=" + news + ", author=" + author
				+ ", comments=" + comments + ", tags=" + tags + "]";
	}

	@Override
	public int hashCode() {
		int hash = 17, result = 0;;
		result = hash  + ((author == null) ? 0 : author.hashCode());
		result = hash * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = hash + ((news == null) ? 0 : news.hashCode());
		result = hash * result+ ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComplexNews other = (ComplexNews) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

}
