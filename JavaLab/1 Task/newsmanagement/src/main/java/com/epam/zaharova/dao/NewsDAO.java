package com.epam.zaharova.dao;

import java.util.List;

import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.News;

/**
 * Provides extra operations for manipulate News data from the data source
 * 
 *
 */
public interface NewsDAO extends AbstractDAO<News, Long> {
	/**
	 * Returns the news by author identifier
	 * 
	 * @param authorId
	 *            the author identifier
	 * @return the news which was found
	 * @throws DAOException
	 */
	News readNewsByAuthor(Long authorId) throws DAOException;

	/**
	 * Returns the list of news by tags identifiers
	 * 
	 * @param tagsId
	 *            list of tags identifiers
	 * @return the list of news
	 * @throws DAOException
	 */
	List<News> readNewsByTags(List<Long> tagsId) throws DAOException;

	/**
	 * Adds new tag to news
	 * 
	 * @param newsId
	 *            news identifier
	 * @param tagId
	 *            tag identifier
	 * @throws DAOException
	 */
	void addNewsTag(Long newsId, Long tagId) throws DAOException;

	/**
	 * Adds new author to news
	 * 
	 * @param newsId
	 *            news identifier
	 * @param authorId
	 *            author identifier
	 * @throws DAOException
	 */
	void addNewsAuthor(Long newsId, Long authorId) throws DAOException;

	/**
	 * Reads the most commented news
	 * 
	 * @return list of the most commented news
	 * @throws DAOException
	 */
	List<News> readMostCommentedNews() throws DAOException;
}
