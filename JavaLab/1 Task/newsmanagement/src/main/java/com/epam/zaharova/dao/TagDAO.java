package com.epam.zaharova.dao;

import java.util.List;

import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Tag;

/**
 * Provides extra operations for manipulate tag data from the data source
 * 
 *
 */
public interface TagDAO extends AbstractDAO<Tag, Long> {
	/**
	 * Receives the list of tags by news identifier
	 * 
	 * @param newsId
	 *            news identifier
	 * @return the list of tags
	 * @throws DAOException
	 */
	List<Tag> readNewsTag(Long newsId) throws DAOException;

	/**
	 * Reads the tag by tag name
	 * 
	 * @param tagName
	 *            tag name
	 * @return the tag
	 * @throws DAOException
	 */
	Tag readTagByName(String tagName) throws DAOException;
}
