package com.epam.zaharova.dao;

import java.util.List;

import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Comment;

/**
 * 
 * Provides extra operations for manipulate comments data from the data source
 *
 */
public interface CommentDAO extends AbstractDAO<Comment, Long> {

	/**
	 * Receives the list of comments of certain news
	 * 
	 * @param newsId
	 *            news identifier
	 * @return the list of comments
	 * @throws DAOException
	 */
	List<Comment> readNewsComments(Long newsId) throws DAOException;
}
