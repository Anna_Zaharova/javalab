package com.epam.zaharova.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.zaharova.dao.TagDAO;
import com.epam.zaharova.entity.Tag;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImpTest {
	@Mock
	private TagDAO tagDAO;
	private TagServiceImp tagService;
	private Tag tag;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		tagService = new TagServiceImp();
		tag = new Tag(1l, "Java");
		tagService.setTagDAO(tagDAO);
	}

	@Test
	public void getById() throws Exception {
		tagService.getTagById(tag.getId());
		verify(tagDAO).read(tag.getId());
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void update() throws Exception {
		tagService.updateTag(tag);
		verify(tagDAO).update(tag);
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void delete() throws Exception {
		tagService.deleteTagById(tag.getId());
		verify(tagDAO).delete(tag.getId());
		verifyNoMoreInteractions(tagDAO);
	}

	@Test
	public void create() throws Exception {
		tagService.addTag(tag);
		verify(tagDAO).create(tag);
		verifyNoMoreInteractions(tagDAO);
	}
}
