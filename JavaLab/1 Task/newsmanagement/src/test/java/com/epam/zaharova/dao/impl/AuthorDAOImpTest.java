package com.epam.zaharova.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.zaharova.dao.AuthorDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class AuthorDAOImpTest {

	private static AuthorDAO authorDAO;
	private static ApplicationContext ac;

	@BeforeClass
	public static void setUpAuthorDAO() {
		ac = new ClassPathXmlApplicationContext("context-test.xml");
		authorDAO = (AuthorDAO) ac.getBean("authorDAO");
	}

	@AfterClass
	public static void closeContext() {
		((ConfigurableApplicationContext) ac).close();
	}

	@Test
	public void insert() throws DAOException {
		Author author = new Author();
		author.setName("Vatja");
		authorDAO.create(author);
		assertTrue(author.getId() > 0);
		author = authorDAO.read(author.getId());
		assertEquals(author.getName(), "Vatja");
	}

	@Test
	public void readById() throws DAOException {
		Author author = authorDAO.read(1l);
		assertTrue(author.getId() == 1l);
		assertEquals(author.getName(), "Petr");
	}

	@Test
	public void update() throws DAOException {
		Author author = new Author();
		author.setName("Alex");
		author.setId(1l);
		authorDAO.update(author);
		author = authorDAO.read(1l);
		assertEquals(author.getName(), "Alex");
	}

	@Test
	public void delete() throws DAOException {
		authorDAO.delete(4l);
	}

	@Test
	public void setExpired() throws DAOException {
		Author author = authorDAO.read(1l);
		Date date = new Date();
		author.setExpired(date);
		authorDAO.setExpired(author);
		assertEquals((authorDAO.read(1l)).getExpired().getTime(),
				date.getTime());
	}

	@Test
	public void readNewsAuthor() throws DAOException {
		Author author = authorDAO.readNewsAuthor(1l);
		assertEquals(author.getName(), "Helen");
	}
}
