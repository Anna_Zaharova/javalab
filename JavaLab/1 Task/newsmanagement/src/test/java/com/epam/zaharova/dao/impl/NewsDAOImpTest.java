package com.epam.zaharova.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.zaharova.dao.NewsDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class NewsDAOImpTest {
	private static NewsDAO newsDAO;
	private static ApplicationContext ac;

	@BeforeClass
	public static void setUpAuthorDAO() {
		ac = new ClassPathXmlApplicationContext("context-test.xml");
		newsDAO = (NewsDAO) ac.getBean("newsDAO");
	}

	@AfterClass
	public static void closeContext() {
		((ConfigurableApplicationContext) ac).close();
	}

	@Test
	public void readById() throws DAOException {
		News news = newsDAO.read(1l);
		assertTrue(news.getId() == 1l);
		assertEquals(news.getTitle(), "title1");
		assertEquals(news.getCreationDate().toString(), "2015-03-02 12:10:10.0");
		assertEquals(news.getFullText(), "full_text1");
		assertEquals(news.getShortText(), "short_text1");

	}

	@Test
	public void create() throws DAOException {
		News news = new News();
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		news.setFullText("test full text");
		news.setShortText("test short text");
		news.setTitle("test title");
		newsDAO.create(news);
		assertTrue(news.getId() > 0);
		news = newsDAO.read(news.getId());
		assertEquals(news.getTitle(), "test title");
		assertEquals(news.getFullText(), "test full text");
		assertEquals(news.getShortText(), "test short text");
	}

	@Test
	public void update() throws DAOException {
		News news = newsDAO.read(2l);
		Date oldDate = news.getModificationDate();
		String newTitle = "new title from update test";
		news.setTitle(newTitle);
		news.setModificationDate(new Date());
		newsDAO.update(news);
		news = newsDAO.read(2l);
		assertTrue(news.getModificationDate().getTime() > oldDate.getTime());
		assertEquals(newTitle, news.getTitle());
	}

	@Test(expected = DAOException.class)
	public void delete() throws DAOException {
		newsDAO.delete(3l);
	}

	@Test
	public void readNewsByAuthor() throws DAOException {
		News news = newsDAO.readNewsByAuthor(2l);
		assertTrue(news.getId() == 1l);
	}

	@Test
	public void readMostCommented() throws DAOException {
		List<News> news = newsDAO.readMostCommentedNews();
		assertTrue(news.size() == 2);
	}

	@Test
	public void readByTags() throws DAOException {
		List<Long> tags = new ArrayList<>();
		tags.add(1l);
		tags.add(3l);
		List<News> news = newsDAO.readNewsByTags(tags);
		assertEquals(news.size(), 2);
	}

	@Test
	public void readAll() throws DAOException {
		List<News> news = newsDAO.readAll();
		assertEquals(news.size(), 3);
	}
}
