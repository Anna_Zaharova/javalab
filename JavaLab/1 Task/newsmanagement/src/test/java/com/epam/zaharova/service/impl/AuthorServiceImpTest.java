package com.epam.zaharova.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.zaharova.dao.AuthorDAO;
import com.epam.zaharova.entity.Author;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImpTest {
	@Mock
	private AuthorDAO authorDAO;
	private AuthorServiceImp authorService;
	private Author author;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		authorService = new AuthorServiceImp();
		author = new Author(1l, "Java");
		authorService.setAuthorDAO(authorDAO);
	}

	@Test
	public void getById() throws Exception {

		when(authorDAO.read(1l)).thenReturn(author);
		Author nAuthor = authorService.getAuthorById(1l);
		assertEquals("Java", nAuthor.getName());
		verify(authorDAO).read(1l);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void update() throws Exception {
		authorService.updateAuthor(author);
		verify(authorDAO).update(author);
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void delete() throws Exception {
		authorService.deleteAuthorById(author.getId());
		verify(authorDAO).delete(author.getId());
		verifyNoMoreInteractions(authorDAO);
	}

	@Test
	public void create() throws Exception {
		authorService.addAuthor(author);
		verify(authorDAO).create(author);
		verifyNoMoreInteractions(authorDAO);
	}

}
