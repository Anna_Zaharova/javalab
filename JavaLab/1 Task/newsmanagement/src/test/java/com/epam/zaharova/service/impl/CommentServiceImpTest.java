package com.epam.zaharova.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.zaharova.dao.CommentDAO;
import com.epam.zaharova.entity.Comment;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImpTest {
	@Mock
	private CommentDAO commentDAO;
	private CommentServiceImp commentService;
	private Comment comment;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		commentService = new CommentServiceImp();
		comment = new Comment("comment", new Date());
		comment.setId(1l);
		commentService.setCommentDAO(commentDAO);
	}

	@Test
	public void getCommentById() throws Exception {
		commentService.getCommentById(comment.getId());
		verify(commentDAO).read(comment.getId());
		verifyNoMoreInteractions(commentDAO);
	}

	@Test
	public void delete() throws Exception {
		commentService.deleteCommentById(comment.getId());
		verify(commentDAO).delete(comment.getId());
		verifyNoMoreInteractions(commentDAO);
	}

	@Test
	public void update() throws Exception {
		commentService.updateComment(comment);
		verify(commentDAO).update(comment);
		verifyNoMoreInteractions(commentDAO);
	}

	@Test
	public void create() throws Exception {
		commentService.addComment(comment);
		verify(commentDAO).create(comment);
		verifyNoMoreInteractions(commentDAO);
	}

	@Test
	public void getNewsComments() throws Exception {
		commentService.getNewsComments(8l);
		verify(commentDAO).readNewsComments(8l);
		verifyNoMoreInteractions(commentDAO);
	}

}
