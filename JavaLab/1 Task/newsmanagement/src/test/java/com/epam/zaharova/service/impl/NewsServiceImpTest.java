package com.epam.zaharova.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.zaharova.dao.NewsDAO;
import com.epam.zaharova.entity.News;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImpTest {
	@Mock
	private NewsDAO newsDAO;
	private NewsServiceImp newsService;
	private News news;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		newsService = new NewsServiceImp();
		news = new News();
		news.setCreationDate(new Date());
		news.setFullText("full text");
		news.setId((long) 12);
		news.setShortText("short text");
		news.setTitle("title");
		newsService.setNewsDAO(newsDAO);
	}

	@Test
	public void create() throws Exception {
		newsService.addNews(news);
		verify(newsDAO).create(news);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void read() throws Exception {
		newsService.getNewsById(news.getId());
		verify(newsDAO).read(news.getId());
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void update() throws Exception {
		newsService.updateNews(news);
		verify(newsDAO).update(news);
		verifyNoMoreInteractions(newsDAO);
	}

	@Test
	public void delete() throws Exception {
		newsService.deleteNewsById(news.getId());
		verify(newsDAO).delete(news.getId());
		verifyNoMoreInteractions(newsDAO);
	}
}
