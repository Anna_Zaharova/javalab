package com.epam.zaharova.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.zaharova.entity.Author;
import com.epam.zaharova.entity.Comment;
import com.epam.zaharova.entity.ComplexNews;
import com.epam.zaharova.entity.News;
import com.epam.zaharova.entity.Tag;
import com.epam.zaharova.service.AuthorService;
import com.epam.zaharova.service.CommentService;
import com.epam.zaharova.service.NewsService;
import com.epam.zaharova.service.TagService;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceImpTest {

	@Mock
	private NewsService newsService;
	@Mock
	private CommentService commentService;
	@Mock
	private TagService tagService;
	@Mock
	private AuthorService authorService;
	private NewsManagementServiceImp newsManagementService;
	private ComplexNews complexNews;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		newsManagementService = new NewsManagementServiceImp();
		newsManagementService.setAuthorService(authorService);
		newsManagementService.setCommentService(commentService);
		newsManagementService.setNewsService(newsService);
		newsManagementService.setTagService(tagService);
		initComplexNews();
	}

	private List<Tag> createTagList() {
		List<Tag> tags = new ArrayList<>();
		tags.add(new Tag(1l, "tag1"));
		tags.add(new Tag(2l, "tag2"));
		tags.add(new Tag(3l, "tag3"));
		return tags;
	}

	private List<Comment> createCommentsList() {
		List<Comment> comments = new ArrayList<>();
		for (int i = 1; i < 5; i++) {
			Comment comment = new Comment();
			comment.setId((long) i);
			comment.setCommentText("some_comment" + i);
			comment.setCreationDate(new Date());
			comment.setNewsId((long) i);
			comments.add(comment);
		}
		return comments;
	}

	private News createNews() {
		News news = new News();
		news.setCreationDate(new Date());
		news.setFullText("full_text");
		news.setId(1l);
		news.setModificationDate(new Date());
		news.setTitle("title");
		news.setShortText("short_text");
		return news;
	}

	private List<News> createNewsList() {
		List<News> news = new ArrayList<News>();
		news.add(createNews());
		return news;
	}

	private void initComplexNews() {
		complexNews = new ComplexNews();
		complexNews.setAuthor(new Author(1l, "Vlad"));
		complexNews.setTags(createTagList());
		complexNews.setComments(createCommentsList());
		complexNews.setNews(createNews());
	}

	@Test
	public void addNewsWithAuthorAndTags() throws Exception {
		newsManagementService.addNewsWithAuthorAndTags(complexNews);
		verify(newsService).addNews(complexNews.getNews());
		verify(newsService).addNewsAuthor(complexNews.getNews().getId(),
				complexNews.getAuthor().getId());
		verify(authorService).addAuthor(complexNews.getAuthor());
		verifyNoMoreInteractions(authorService);
		List<Tag> tags = complexNews.getTags();
		for (int i = 0; i < 3; i++) {
			verify(tagService).addTag(complexNews.getTags().get(i));
			verify(newsService).addNewsTag(complexNews.getNews().getId(),
					tags.get(i).getId());
		}
		verifyNoMoreInteractions(newsService);
		verifyNoMoreInteractions(tagService);
	}

	@Test
	public void getComplexNews() throws Exception {
		newsManagementService.getComplexNews(1l);
		verify(newsService).getNewsById(1l);
		verify(authorService).getNewsAuthor(1l);
		verify(tagService).getNewsTag(1l);
		verify(commentService).getNewsComments(1l);
		verifyNoMoreInteractions(newsService);
		verifyNoMoreInteractions(authorService);
		verifyNoMoreInteractions(tagService);
		verifyNoMoreInteractions(commentService);
	}

	@Test
	public void getMostCommentedComplexNews() throws Exception {
		when(newsService.getMostCommentedNews()).thenReturn(createNewsList());
		newsManagementService.getMostCommentedComplexNews();
		verify(newsService).getMostCommentedNews();
		verify(tagService).getNewsTag(1l);
		verify(authorService).getNewsAuthor(1l);
		verify(commentService).getNewsComments(1l);
		verifyNoMoreInteractions(newsService);
		verifyNoMoreInteractions(tagService);
		verifyNoMoreInteractions(commentService);
		verifyNoMoreInteractions(authorService);
	}

	@Test
	public void deleteComplexNews() throws Exception {
		newsManagementService.deleteComplexNews(complexNews);
		verify(newsService).deleteNewsById(complexNews.getNews().getId());
		verifyNoMoreInteractions(newsService);
		verify(authorService).setExpired(complexNews.getAuthor());
		verifyNoMoreInteractions(authorService);
		List<Tag> tags = complexNews.getTags();
		for (int i = 0; i < 3; i++) {
			verify(tagService).deleteTagById(tags.get(i).getId());
		}
		verifyNoMoreInteractions(tagService);
		List<Comment> comments = complexNews.getComments();
		for (int i = 0; i < 4; i++) {
			verify(commentService).deleteCommentById(comments.get(i).getId());
		}
		verifyNoMoreInteractions(commentService);
	}
}
