package com.epam.zaharova.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.zaharova.dao.CommentDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class CommentDAOImpTest {
	private static CommentDAO commentDAO;
	private static ApplicationContext ac;

	@BeforeClass
	public static void setUpAuthorDAO() {
		ac = new ClassPathXmlApplicationContext("context-test.xml");
		commentDAO = (CommentDAO) ac.getBean("commentDAO");
	}

	@AfterClass
	public static void closeContext() {
		((ConfigurableApplicationContext) ac).close();
	}

	@Test
	public void create() throws DAOException {
		Comment comment = new Comment();
		comment.setCommentText("HI");
		Date date = new Date();
		comment.setCreationDate(date);
		comment.setNewsId(1l);
		commentDAO.create(comment);
		assertTrue(comment.getId() > 0);
		comment = commentDAO.read(comment.getId());
		assertEquals(comment.getCommentText(), "HI");
		assertEquals(comment.getCreationDate().getTime(), date.getTime());
		assertEquals(comment.getNewsId(), 1l);
	}

	@Test
	public void update() throws DAOException {
		Comment comment = commentDAO.read(2l);
		comment.setCommentText("someText");
		commentDAO.update(comment);
		comment = commentDAO.read(2l);
		assertEquals("someText", comment.getCommentText());
	}

	@Test
	public void delete() throws DAOException {
		commentDAO.delete(2l);
	}

	@Test
	public void read() throws DAOException {
		Comment comment = commentDAO.read(1l);
		assertEquals("Cool!", comment.getCommentText());
		assertTrue(comment.getId() == 1l);
	}

	@Test
	public void readNewsComment() throws DAOException {
		List<Comment> comments = commentDAO.readNewsComments(2l);
		assertTrue(comments.size() == 2);
		Comment comment = comments.get(0);
		assertTrue(comment.getId() == 4l);
		assertEquals(comment.getCommentText(), "Difficult");
	}
}
