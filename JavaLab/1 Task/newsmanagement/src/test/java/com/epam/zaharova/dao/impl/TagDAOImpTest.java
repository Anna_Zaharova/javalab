package com.epam.zaharova.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.zaharova.dao.TagDAO;
import com.epam.zaharova.dao.exception.DAOException;
import com.epam.zaharova.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context-test.xml" })
@DatabaseSetup(value = "classpath:news-data.xml", type = DatabaseOperation.CLEAN_INSERT)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class TagDAOImpTest {

	private static TagDAO tagDAO;
	private static ApplicationContext ac;

	@BeforeClass
	public static void setUpTagDAO() {
		ac = new ClassPathXmlApplicationContext("classpath:context-test.xml");
		tagDAO = (TagDAO) ac.getBean("tagDAO");
	}

	@AfterClass
	public static void closeContext() {
		((ConfigurableApplicationContext) ac).close();
	}

	@Test
	public void readById() throws DAOException {
		Tag tag = tagDAO.read(3l);
		assertTrue(tag.getId() == 3l);
		assertEquals("Python", tag.getName());
	}

	@Test
	public void update() throws DAOException {
		Tag tag = new Tag(1l, "Test");
		tagDAO.update(tag);
		Tag tag1 = tagDAO.read(1l);
		assertEquals("Test", tag1.getName());
	}

	@Test
	public void readByName() throws DAOException {
		Tag tag = tagDAO.readTagByName(".NET");
		assertEquals(".NET", tag.getName());
	}

	@Test(expected = DAOException.class)
	public void delete() throws DAOException {
		tagDAO.delete(4l);
	}

	@Test
	public void insert() throws DAOException {
		Tag tag = new Tag("OOP");
		tagDAO.create(tag);
		assertTrue(tag.getId() > 0);
		tag = tagDAO.read(tag.getId());
		assertEquals(tag.getName(), "OOP");
	}
}
