INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Petja');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Vasja');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Andrey');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Roma');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Elena');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Olga');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Vanja');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Ekaterina');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Alex');
INSERT INTO Author(author_id, author_name) VALUES(author_seq.nextval, 'Ilona');

INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Music');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Video');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Policy');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Society');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Hospital');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Medicine');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Crime');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Militia');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Murder');
INSERT INTO Tag(tag_id, tag_name) VALUES(tag_seq.nextval, 'Cars');

INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text1', 'full_text1', 'title1', TO_DATE('2015-02-12 16:12:00','YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-12 16:12:00','YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text2', 'full_text2', 'title2', TO_DATE('2015-02-13 16:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-12 16:12:00','YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text3', 'full_text3', 'title3', TO_DATE('2015-02-14 16:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-12 16:12:00','YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text4', 'full_text4', 'title4',  TO_DATE('2015-02-13 16:12:00', 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE('2015-02-13 16:12:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text5', 'full_text5', 'title5', TO_DATE('2015-03-03 16:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-03 16:12:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text6', 'full_text6', 'title6', TO_DATE('2015-02-25 16:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-12 16:12:00','YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text7', 'full_text7', 'title7', TO_DATE('2015-03-18 16:12:00', 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE('2015-03-18 16:12:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text8', 'full_text8', 'title8',  TO_DATE('2015-03-17 16:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-03-17 16:12:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text9', 'full_text9', 'title9',  TO_DATE('2015-02-16 16:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-12 16:12:00','YYYY-MM-DD HH24:MI:SS'));
INSERT INTO News(news_id, short_text, full_text, title, creation_date, modification_date)
VALUES(news_seq.nextval, 'short_text10', 'full_text10', 'title10', TO_DATE('2015-02-17 16:12:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-02-12 16:12:00','YYYY-MM-DD HH24:MI:SS'));

INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text1', TO_DATE('2015-04-12 17:10:00','YYYY-MM-DD HH24:MI:SS'), 24);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text2', TO_DATE('2015-04-12 18:18:00','YYYY-MM-DD HH24:MI:SS'), 24);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text3', TO_DATE('2015-04-12 17:10:00','YYYY-MM-DD HH24:MI:SS'), 24);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text4', TO_DATE('2015-04-12 21:30:00','YYYY-MM-DD HH24:MI:SS'), 25);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text5', TO_DATE('2015-04-12 09:19:00','YYYY-MM-DD HH24:MI:SS'), 26);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text6', TO_DATE('2015-04-12 08:15:00','YYYY-MM-DD HH24:MI:SS'), 27);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text7', TO_DATE('2015-04-12 13:13:00','YYYY-MM-DD HH24:MI:SS'), 28);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text8', TO_DATE('2015-04-12 18:19:00','YYYY-MM-DD HH24:MI:SS'), 29);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text9', TO_DATE('2015-04-12 10:16:00','YYYY-MM-DD HH24:MI:SS'), 30);
INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) 
VALUES(comments_seq.nextval,'comment_text10', TO_DATE('2015-04-12 18:11:00','YYYY-MM-DD HH24:MI:SS'), 31);

INSERT INTO news_author(news_id, author_id) VALUES(24,41);
INSERT INTO news_author(news_id, author_id) VALUES(25, 42);
INSERT INTO news_author(news_id, author_id) VALUES(26,43);
INSERT INTO news_author(news_id, author_id) VALUES(27,44);
INSERT INTO news_author(news_id, author_id) VALUES(28,45);
INSERT INTO news_author(news_id, author_id) VALUES(29,46);
INSERT INTO news_author(news_id, author_id) VALUES(30,47);
INSERT INTO news_author(news_id, author_id) VALUES(31,48);
INSERT INTO news_author(news_id, author_id) VALUES(32,49);
INSERT INTO news_author(news_id, author_id) VALUES(33,50);

INSERT INTO news_tag(news_id, tag_id) VALUES(24,82);
INSERT INTO news_tag(news_id, tag_id) VALUES(24,83);
INSERT INTO news_tag(news_id, tag_id) VALUES(24,84);
INSERT INTO news_tag(news_id, tag_id) VALUES(25,82);
INSERT INTO news_tag(news_id, tag_id) VALUES(26,87);
INSERT INTO news_tag(news_id, tag_id) VALUES(27,88);
INSERT INTO news_tag(news_id, tag_id) VALUES(28,82);
INSERT INTO news_tag(news_id, tag_id) VALUES(29,82);
INSERT INTO news_tag(news_id, tag_id) VALUES(30,82);
INSERT INTO news_tag(news_id, tag_id) VALUES(31,86);
INSERT INTO news_tag(news_id, tag_id) VALUES(32,87);
INSERT INTO news_tag(news_id, tag_id) VALUES(32,88);
INSERT INTO news_tag(news_id, tag_id) VALUES(33,90);