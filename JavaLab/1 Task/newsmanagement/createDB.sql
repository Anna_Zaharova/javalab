CREATE TABLE ZAHAROVA.AUTHOR
  (
    AUTHOR_ID NUMBER(20) NOT NULL PRIMARY KEY,
    AUTHOR_NAME NVARCHAR2(30) NOT NULL ,
    EXPIRED TIMESTAMP
  );
CREATE SEQUENCE "ZAHAROVA"."AUTHOR_SEQ" MINVALUE 1 INCREMENT BY 1 START WITH 1;
  CREATE TABLE ZAHAROVA.NEWS
    (
      NEWS_ID NUMBER(20) NOT NULL PRIMARY KEY,
      SHORT_TEXT NVARCHAR2(100) NOT NULL ,
      FULL_TEXT NVARCHAR2(2000) NOT NULL ,
      TITLE NVARCHAR2(30) NOT NULL ,
      CREATION_DATE     TIMESTAMP NOT NULL ,
      MODIFICATION_DATE DATE NOT NULL
    );
CREATE SEQUENCE "ZAHAROVA"."NEWS_SEQ" MINVALUE 1 INCREMENT BY 1 START WITH 1;
  CREATE TABLE ZAHAROVA.COMMENTS
    (
      COMMENT_ID NUMBER(20) NOT NULL PRIMARY KEY,
      COMMENT_TEXT NVARCHAR2(100) NOT NULL ,
      CREATION_DATE TIMESTAMP NOT NULL ,
      NEWS_ID       NUMBER(20) NOT NULL,
      CONSTRAINT NEWS_COMMENTS_FK FOREIGN KEY(NEWS_ID) REFERENCES ZAHAROVA.NEWS(NEWS_ID)
    );
CREATE SEQUENCE "ZAHAROVA"."COMMENTS_SEQ" MINVALUE 1 INCREMENT BY 1 START WITH 1;
  CREATE TABLE ZAHAROVA.TAG
    (
      TAG_ID NUMBER(20) NOT NULL PRIMARY KEY,
      TAG_NAME NVARCHAR2(30) NOT NULL
    );
CREATE SEQUENCE "ZAHAROVA"."TAG_SEQ" MINVALUE 1 INCREMENT BY 1 START WITH 1;
  CREATE TABLE ZAHAROVA.NEWS_AUTHOR
    (
      NEWS_ID   NUMBER(20) NOT NULL ,
      AUTHOR_ID NUMBER(20) NOT NULL,
      CONSTRAINT NEWS_AUTHOR_FK FOREIGN KEY(NEWS_ID)REFERENCES ZAHAROVA.NEWS(NEWS_ID),
      CONSTRAINT AUTHOR_NEWS_FK FOREIGN KEY(AUTHOR_ID)REFERENCES ZAHAROVA.AUTHOR(AUTHOR_ID)
    );
  CREATE TABLE ZAHAROVA.NEWS_TAG
    (
      NEWS_ID NUMBER(20) NOT NULL ,
      TAG_ID  NUMBER(20) NOT NULL ,
      CONSTRAINT NEWS_TAG_FK FOREIGN KEY(NEWS_ID)REFERENCES ZAHAROVA.NEWS(NEWS_ID),
      CONSTRAINT TAG_NEWS_FK FOREIGN KEY(TAG_ID)REFERENCES ZAHAROVA.TAG(TAG_ID)
    );
  CREATE TABLE "ZAHAROVA"."USERS"
    (
      USER_ID NUMBER(20) NOT NULL PRIMARY KEY,
      USER_NAME NVARCHAR2(50) NOT NULL,
      LOGIN NVARCHAR2(30) NOT NULL,
      PASSWORD NVARCHAR2(30) NOT NULL
    );
CREATE SEQUENCE "ZAHAROVA"."USER_SEQ" MINVALUE 1 INCREMENT BY 1 START WITH 1;
  CREATE TABLE "ZAHAROVA"."ROLES"
    (
      USER_ID NUMBER(20) NOT NULL,
      ROLE_NAME NVARCHAR2(50) NOT NULL,
      CONSTRAINT ROLES_USER_FK FOREIGN KEY (USER_ID) REFERENCES  ZAHAROVA."USERS"(USER_ID)
    );